import time
import paho.mqtt.client as mqtt
import json

def on_connect(client, userdata, flags, rc):
    print("Connected with result code " + str(rc))

def on_message(client, userdata, msg):
    print(msg.topic+" "+str(msg.payload))

client = mqtt.Client()
client.on_connect = on_connect
client.on_message = on_message

client.connect("192.168.1.6", 1883, 60)
#client.subscribe("wifi2mqtt/develop/status")

client.loop_start()

timeout=1

set_cmd={
  "state":"ON"
}

light_set={
  "state":"ON",
  "transition":1000,
   "segments":[
        '{\"sPixel\": 0,   \"ePixel\": 5,   \"h\": 0, \"s\":1, \"l\":0.6 }',
        '{\"sPixel\": 5,  \"ePixel\": 10,   \"h\": 100, \"s\":1, \"l\":0.6 }'
    ]
}

while True:
    set_cmd["state"] = "ON"
    light_set["state"] = "ON"
    print("SET: " + json.dumps(set_cmd))
    client.publish("wifi2mqtt/develop_2159d/relais1/set", json.dumps(set_cmd))
    time.sleep(timeout)
    client.publish("wifi2mqtt/develop_2159d/relais2/set", json.dumps(set_cmd))
    time.sleep(timeout)
    client.publish("wifi2mqtt/develop_2159d/relais3/set", json.dumps(set_cmd))
    time.sleep(timeout)
    client.publish("wifi2mqtt/develop_2159d/relais4/set", json.dumps(set_cmd))
    time.sleep(timeout)
    client.publish("wifi2mqtt/develop_2159d/Light/set", json.dumps(light_set))
    time.sleep(timeout)

    set_cmd["state"] = "OFF"
    light_set["state"] = "OFF"
    print("SET: " + json.dumps(set_cmd))
    client.publish("wifi2mqtt/develop_2159d/relais1/set", json.dumps(set_cmd))
    time.sleep(timeout)
    client.publish("wifi2mqtt/develop_2159d/relais2/set", json.dumps(set_cmd))
    time.sleep(timeout)
    client.publish("wifi2mqtt/develop_2159d/relais3/set", json.dumps(set_cmd))
    time.sleep(timeout)
    client.publish("wifi2mqtt/develop_2159d/relais4/set", json.dumps(set_cmd))
    time.sleep(timeout)
    client.publish("wifi2mqtt/develop_2159d/Light/set", json.dumps(light_set))
    time.sleep(timeout)