import time
import paho.mqtt.client as mqtt
import json

def on_connect(client, userdata, flags, rc):
    print("Connected with result code " + str(rc))

def on_message(client, userdata, msg):
    print(msg.topic+" "+str(msg.payload))

client = mqtt.Client()
client.on_connect = on_connect
client.on_message = on_message

client.connect("192.168.1.6", 1883, 60)
client.subscribe("wifi2mqtt/develop/status")

client.loop_start()

set_cmd={
  "state":"ON",
  "brightness": 10,
  "transition": 1000
}

client.publish("wifi2mqtt/develop/set", json.dumps(set_cmd))

set_cmd["brightness"] = 200
set_cmd["state"] = "ON"
print("SET: " + json.dumps(set_cmd))
client.publish("wifi2mqtt/develop/set", json.dumps(set_cmd))

set_cmd["brightness"] = 20
set_cmd["state"] = "OFF"
print("SET: " + json.dumps(set_cmd))
client.publish("wifi2mqtt/develop/set", json.dumps(set_cmd))

set_cmd["brightness"] = 255
set_cmd["state"] = "ON"
print("SET: " + json.dumps(set_cmd))
client.publish("wifi2mqtt/develop/set", json.dumps(set_cmd))