#!/usr/bin/env python
import time
import paho.mqtt.client as mqtt
import json

def on_connect(client, userdata, flags, rc):
    print("Connected with result code " + str(rc))

def on_message(client, userdata, msg):
    print(msg.topic+" "+str(msg.payload))

test_topic="wifi2mqtt/ligth_wheel/light"

client = mqtt.Client()
client.on_connect = on_connect
client.on_message = on_message

client.connect("192.168.1.6", 1883, 60)
client.subscribe("wifi2mqtt/ligth_wheel")

client.loop_start()

conf_cmd={\
  "nLeds"         : 130,\
  "client_name"   : "mickey",\
  "wlan_ssid"     : "Virus-EG",\
  "wlan_key"      : "sebastiaan.wielens",\
  "wlan_gateway"  : "192.168.1.2",\
  "mqtt_broker_ip": "192.168.1.6",\
  "base_topic"    : "wifi2mqtt/mickey",\
  "mqtt_mode"     : "MQTT_CLIENT"\
}

set_cmd={
  "state":"ON", 
  "transition": 500,
  "segments": [
    { "sPixel": 0,     "ePixel": 217,  "h": 0,     "s":0.5,   "l":1 , "w":0},
    { "sPixel": 217,   "ePixel": 433,  "h": 0,     "s":0.5,   "l":1,  "w":0}
   ]
}

wave_cmd={
	"t_mode": "WAVE_TAIL_OLD",
	"c_mode": "WAVE_MODE_COLOR",
	"sPixel": 0,
    "ePixel": 130,
	"step_ms": 10,
	"pattern": "[\
		'{ \"wrgb\": \"0xffff\",   \"brightness\": 20}',\
		'{ \"wrgb\": \"0xffff\",   \"brightness\": 40}',\
		'{ \"wrgb\": \"0xffff\",   \"brightness\": 70}',\
		'{ \"wrgb\": \"0xffff\",   \"brightness\": 100}',\
		'{ \"wrgb\": \"0xffff\",   \"brightness\": 150}',\
		'{ \"wrgb\": \"0xffff\",   \"brightness\": 120}'\
	]"
}

seed_cmd={
	"step_ms" : 10,\
	"step_cnt": 2,\
	"seeds" : "[\
		'{\"brightness\":10, \"sPixel\": 0,  \"ePixel\": 50}',\
		'{\"brightness\": 10, \"sPixel\": 130,  \"ePixel\": 50}'\
	]"\
}

while True:
    #print("SET: " + json.dumps(set_cmd))
    set_cmd["state"]="ON"
    set_cmd["segments"][0]["h"]=0
    set_cmd["segments"][0]["l"]=1
    set_cmd["segments"][0]["w"]=0
    set_cmd["segments"][1]["h"]=100
    set_cmd["segments"][1]["l"]=1
    set_cmd["segments"][1]["w"]=0
    print("SET: " + json.dumps(set_cmd))
    client.publish(test_topic + "/set", json.dumps(set_cmd))
    time.sleep(2)

    #print("SET: " + json.dumps(set_cmd))
    set_cmd["state"]="ON"
    set_cmd["segments"][0]["h"]=100
    set_cmd["segments"][1]["h"]=0
    print("SET: " + json.dumps(set_cmd))
    client.publish(test_topic + "/set", json.dumps(set_cmd))
    time.sleep(2)
	
	#print("SET: " + json.dumps(set_cmd))
    set_cmd["state"]="ON"
    set_cmd["segments"][0]["h"]=234
    set_cmd["segments"][1]["h"]=100
    print("SET: " + json.dumps(set_cmd))
    client.publish(test_topic + "/set", json.dumps(set_cmd))
    time.sleep(2)
	
	#print("SET: " + json.dumps(set_cmd))
    set_cmd["state"]="ON"
    set_cmd["segments"][0]["h"]=0
    set_cmd["segments"][1]["h"]=234
    print("SET: " + json.dumps(set_cmd))
    client.publish(test_topic + "/set", json.dumps(set_cmd))
    time.sleep(2)
	
	#print("SET: " + json.dumps(set_cmd))
    set_cmd["state"]="ON"
    set_cmd["segments"][0]["h"]=0
    set_cmd["segments"][0]["l"]=0
    set_cmd["segments"][0]["w"]=1
    set_cmd["segments"][1]["h"]=234
    set_cmd["segments"][1]["l"]=0
    set_cmd["segments"][1]["w"]=1
    print("SET: " + json.dumps(set_cmd))
    client.publish(test_topic + "/set", json.dumps(set_cmd))
    time.sleep(2)

    #######################################################################

    #seed_cmd["step_cnt"] = 1
    #decoded = json.loads(seed_cmd)
    #seed_cmd["seeds"][0]["sPixel"] = 83
    #seed_cmd["seeds"][0]["ePixel"] = 112
    #print("SEED: " + json.dumps(seed_cmd))
    #client.publish("wifi2mqtt/mickey/seed", json.dumps(seed_cmd))
    #time.sleep(2)

    ########################################################################
#    wave_cmd["c_mode"]="WAVE_MODE_COLOR"
#    wave_cmd["t_mode"] = "WAVE_TAIL_NEW"
#    print("change c_mode to : " + wave_cmd['c_mode'])
#    print("change t_mode to : " + wave_cmd['t_mode'])
#    client.publish("wifi2mqtt/mickey/wave", json.dumps(wave_cmd))
#    time.sleep(1.5)

#    print("RESET")
#    client.publish("wifi2mqtt/mickey/set", json.dumps(set_cmd))
#    time.sleep(1.5)

#    wave_cmd["c_mode"] = "WAVE_MODE_COLOR"
#    wave_cmd["t_mode"] = "WAVE_TAIL_OLD"
#    print("change c_mode to : " + wave_cmd['c_mode'])
#    print("change t_mode to : " + wave_cmd['t_mode'])
#    client.publish("wifi2mqtt/mickey/wave", json.dumps(wave_cmd))
#    time.sleep(1.5)

#    print("RESET")
#    client.publish("wifi2mqtt/mickey/set", json.dumps(set_cmd))
#    time.sleep(1.5)

#    wave_cmd['c_mode'] = "WAVE_MODE_BRIGHTNESS"
#    wave_cmd["t_mode"] = "WAVE_TAIL_NEW"
#    print("change c_mode to : " + wave_cmd['c_mode'])
#    print("change t_mode to : " + wave_cmd['t_mode'])
#    client.publish("wifi2mqtt/mickey/wave", json.dumps(wave_cmd))
#    time.sleep(1.5)

#    print("RESET")
#    client.publish("wifi2mqtt/mickey/set", json.dumps(set_cmd))
#    time.sleep(1.5)

#    wave_cmd['c_mode'] = "WAVE_MODE_BRIGHTNESS"
#    wave_cmd["t_mode"] = "WAVE_TAIL_OLD"
#    print("change c_mode to : " + wave_cmd['c_mode'])
#    print("change t_mode to : " + wave_cmd['t_mode'])
#    client.publish("wifi2mqtt/mickey/wave", json.dumps(wave_cmd))
#    time.sleep(1.5)

#    print("RESET")
#    client.publish("wifi2mqtt/mickey/set", json.dumps(set_cmd))
#    time.sleep(1.5)


