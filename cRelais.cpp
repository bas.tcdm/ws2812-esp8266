/*
 * cRelais.cpp
 *
 *  Created on: 28.12.2020
 *      Author: swielens
 */
#include "cRelais.h"
#include "mqtt.h"
#include "utils.h"
#include "persistent_storage.h"

#define RELAIS_PUBLISH_INT_S			60 //Each 2 minutes

//static int	n_registered_relais=0;
static cRelais *registered_relais[N_RELAIS];

static void _mqttMsgHandler(const String &topic, const String &payload){
	SERIAL_PRINTLN(payload);
	for (int i=0; i< N_RELAIS; i++){
		if(registered_relais[i]!=NULL){
			registered_relais[i]->mqttMsgHandler(topic, payload);
		}
	}
}

cRelais::cRelais(unsigned int e_address, String base_topic, relais_settings_t cfg){
	eeprom_address=e_address;
	elapsed_time_ms=0;

	eeprom_read_buf(eeprom_address, (unsigned char*)&c_eeprom, sizeof(c_eeprom));
	int crc=calculate_crc((unsigned char*)&c_eeprom.layout_version, sizeof(c_eeprom)-sizeof(int));

	parent_topic=base_topic;

	if (crc!=c_eeprom.crc || c_eeprom.crc==0 || c_eeprom.layout_version!=RELAIS_EEPROM_LAYOUT_VERSION){
		//Default values
		SERIAL_PRINTF("Wrong CRC: %i on eeprom_address=%i, eeprom_version=%i\n", crc,eeprom_address, c_eeprom.layout_version);
		c_eeprom.layout_version=RELAIS_EEPROM_LAYOUT_VERSION;
		c_eeprom.settings=cfg;
		c_eeprom.isOn=false;
		base_topic=String(cfg.mqtt_topic);//String("/relais_") + ((int)this & 0xFFFF);
		base_topic.toCharArray((char*)&c_eeprom.settings.mqtt_topic, sizeof(c_eeprom.settings.mqtt_topic));
		c_eeprom.crc=calculate_crc((unsigned char*)&c_eeprom.layout_version, sizeof(c_eeprom)-sizeof(int));
		SERIAL_PRINTF("store Relais config on address=%i, CRC=%i\n", eeprom_address, c_eeprom.crc);
		eepom_write_buf(eeprom_address, (unsigned char*)&c_eeprom, sizeof(c_eeprom));

		pinMode(c_eeprom.settings.gpio, OUTPUT);
		char level=c_eeprom.settings.invert?!c_eeprom.isOn:c_eeprom.isOn;
		digitalWrite(c_eeprom.settings.gpio, level);

	} else if (c_eeprom.settings.gpio >= 0){
		pinMode(c_eeprom.settings.gpio, OUTPUT);
		char level=c_eeprom.settings.invert?!c_eeprom.isOn:c_eeprom.isOn;
		digitalWrite(c_eeprom.settings.gpio, level);
	}
	SERIAL_PRINTF("Relais Object on gpio=%i, mqtt_topic=%s, mqtt_parent_topic=%s, e_address=%i\n", c_eeprom.settings.gpio,c_eeprom.settings.mqtt_topic, parent_topic.c_str(), e_address);
	String msg = "OK: {";
	msg.concat("  \"base_topic\": \""   + String(c_eeprom.settings.mqtt_topic) + "\"");
	msg.concat("  \"parent_topic\": \""   + parent_topic + "\"");
	msg.concat(",  \"gpio\": "   + String(c_eeprom.settings.gpio));
	msg.concat(", \"state\": \""  + String(c_eeprom.isOn?"ON":"OFF") + "\"");
	msg.concat("}");

	mqtt_publish(String(base_topic) + "/response",  msg);

	for (int i=0; i< N_RELAIS; i++){
		if(registered_relais[i]==NULL){
			registered_relais[i]=this;
			SERIAL_PRINTF("Add Relais to index %i\n",i);
			break;
		}
	}
}

cRelais::~cRelais(){
	mqtt_unsubscribe(parent_topic + String(c_eeprom.settings.mqtt_topic) + "/config");
	mqtt_unsubscribe(parent_topic + String(c_eeprom.settings.mqtt_topic) + "/set");

	for (int i=0; i< N_RELAIS; i++){
		if(registered_relais[i]==this){
			registered_relais[i]=NULL;
			SERIAL_PRINTF("Removed Relay from index %i\n",i);
		}
	}
}

void cRelais::reset_config(){
	memset((char*)&c_eeprom, 0, sizeof(c_eeprom));
	eepom_write_buf(eeprom_address, (unsigned char*)&c_eeprom, sizeof(c_eeprom));
}

void cRelais::mqtt_connected(void){
	mqtt_subscribe(parent_topic + String(c_eeprom.settings.mqtt_topic) + String("/config"), _mqttMsgHandler);
	mqtt_subscribe(parent_topic + String(c_eeprom.settings.mqtt_topic) + String("/set"), _mqttMsgHandler);
}

void cRelais::mqtt_disconnected(void){
	mqtt_unsubscribe(parent_topic + String(c_eeprom.settings.mqtt_topic) + "/config");
	mqtt_unsubscribe(parent_topic + String(c_eeprom.settings.mqtt_topic) + "/set");
}

void cRelais::loop_tick(int intervall_ms){
	elapsed_time_ms+=intervall_ms;

	if ((elapsed_time_ms > (RELAIS_PUBLISH_INT_S*1000))){
		String msg="{";
		msg.concat(" \"state\":\"" + String(c_eeprom.isOn?"ON":"OFF") + "\"");
		msg.concat(", \"gpio\": "   + String(c_eeprom.settings.gpio));
		msg.concat("}");
		//SERIAL_PRINTLN(parent_topic + String(c_eeprom.settings.mqtt_topic) + ": " + msg);
		if (mqtt_isConnected()) {
			mqtt_publish(parent_topic + String(c_eeprom.settings.mqtt_topic),msg);
		} else {
			SERIAL_PRINTLN("cRelais Lost connection")
		}

		elapsed_time_ms=0;
	}
}

void cRelais::mqttMsgHandler(const String &topic, const String &payload){
	String mqtt_topic=parent_topic + String(c_eeprom.settings.mqtt_topic);
	if (topic== String(mqtt_topic + "/config")){
		//SERIAL_PRINTLN("for me " + topic + " : \n" + payload);
		auto error = deserializeJson(jsonBuffer, payload);
		if (error) {
			SERIAL_PRINT(F("deserializeJson() failed with code "));
			SERIAL_PRINTLN(error.c_str());
			mqtt_publish(parent_topic + String(c_eeprom.settings.mqtt_topic) + "/response",  String("{ \"code\":\"ERROR\", \"msg\": \"CONFIG: Payload missing or incorrect\"}"));
			return;
		}

		if (!jsonBuffer.containsKey("gpio")) {
			mqtt_publish(parent_topic + String(c_eeprom.settings.mqtt_topic) + "/response",  String("{ \"code\":\"ERROR\", \"msg\": \"Mandatory field gpio missing\"}"));
			SERIAL_PRINTLN("ERROR: Mandatory field gpio missing");
			return;
		}

		if (jsonBuffer.containsKey("device_topic")) {
			String base_topic=jsonBuffer["device_topic"].as<String>();
			if (!base_topic.startsWith("/")) {
				base_topic="/"+ base_topic;
			}

			if (base_topic.length()<MAX_TOPIC_LENTH) {
				mqtt_unsubscribe(parent_topic + String(c_eeprom.settings.mqtt_topic) + "/config");
				mqtt_unsubscribe(parent_topic + String(c_eeprom.settings.mqtt_topic) + "/set");

				base_topic.toCharArray(c_eeprom.settings.mqtt_topic,MAX_TOPIC_LENTH);
				mqtt_subscribe(String(parent_topic + c_eeprom.settings.mqtt_topic) + String("/config"), _mqttMsgHandler);
				mqtt_subscribe(String(parent_topic + c_eeprom.settings.mqtt_topic) + String("/set"), _mqttMsgHandler);
				SERIAL_PRINTF("Configure device_topic: %s\n", c_eeprom.settings.mqtt_topic);
			} else {
				SERIAL_PRINTLN("ERROR: Topic length exceeds maximum " + String(MAX_TOPIC_LENTH));
				mqtt_publish(String(base_topic) + "/response",  String("{ \"code\":\"ERROR\", \"msg\": \"Topic length exceeds maximum " + String(MAX_TOPIC_LENTH) + "\"}"));
			}
		}

		int gpio=jsonBuffer["gpio"].as<int>();
		if (c_eeprom.settings.gpio!=gpio && gpio>=0) {
			digitalWrite(c_eeprom.settings.gpio, 0);
			pinMode(c_eeprom.settings.gpio, INPUT);
			c_eeprom.settings.gpio=gpio;
			pinMode(c_eeprom.settings.gpio, OUTPUT);
			digitalWrite(c_eeprom.settings.gpio, 0);
			c_eeprom.isOn=false;
		}

		c_eeprom.layout_version=RELAIS_EEPROM_LAYOUT_VERSION;

		c_eeprom.crc=calculate_crc((unsigned char*)&c_eeprom.layout_version, sizeof(c_eeprom)-sizeof(int));
		SERIAL_PRINTF("store Relais config on address=%i, CRC=%i\n", eeprom_address, c_eeprom.crc);
		eepom_write_buf(eeprom_address, (unsigned char*)&c_eeprom, sizeof(c_eeprom));

		SERIAL_PRINTF("Set config for %s to gpio=%i, eeprom Layout=%i\n", c_eeprom.settings.mqtt_topic, c_eeprom.settings.gpio, c_eeprom.layout_version);
		mqtt_publish(parent_topic + String(c_eeprom.settings.mqtt_topic) + "/response",  String("{ \"code\":\"OK\", \"msg\": \"CONFIG updated: gpio=") + String(c_eeprom.settings.gpio) + "\"}");
	    return;
	}

	if (topic== String(mqtt_topic + "/set")) {
		auto error = deserializeJson(jsonBuffer, payload);
		if (error) {
			SERIAL_PRINT(F("deserializeJson() failed with code "));
			SERIAL_PRINTLN(error.c_str());
			mqtt_publish(parent_topic + String(c_eeprom.settings.mqtt_topic) + "/response",  String("{ \"code\":\"ERROR\", \"msg\": \"CONFIG: Payload missing or incorrect"));
			return;
		}

		if (!jsonBuffer.containsKey("state")) {
			mqtt_publish(parent_topic + String(c_eeprom.settings.mqtt_topic) + "/response",  String("{ \"code\":\"ERROR\", \"msg\": \"Mandatory field state missing\"}"));
			SERIAL_PRINTLN("ERROR: Mandatory field state missing");
			return;
		}

		String state=jsonBuffer["state"].as<String>();

		c_eeprom.isOn=state=="ON"?true:false;
		char level=c_eeprom.settings.invert?!c_eeprom.isOn:c_eeprom.isOn;
		SERIAL_PRINTF("set gpio %i to state to %i\n",c_eeprom.settings.gpio, level);
		digitalWrite(c_eeprom.settings.gpio, level);

		c_eeprom.crc=calculate_crc((unsigned char*)&c_eeprom.layout_version, sizeof(c_eeprom)-sizeof(int));
		//SERIAL_PRINTF("store Relais config on address=%i, CRC=%i\n", eeprom_address, c_eeprom.crc);
		eepom_write_buf(eeprom_address, (unsigned char*)&c_eeprom, sizeof(c_eeprom));

		SERIAL_PRINTF("Set state for %s to %s\n", c_eeprom.settings.mqtt_topic, c_eeprom.isOn?"ON":"OFF");
		mqtt_publish(parent_topic + String(c_eeprom.settings.mqtt_topic) + "/response",  String("{ \"code\":\"OK\", \"msg\": \"Set State to ") + state + "\"}");
		return;
	}

}


