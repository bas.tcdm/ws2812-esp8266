/*
 * effect_candle.cpp
 *
 *  Created on: 18.12.2021
 *      Author: swielens
 */

#include "cLedStripe.h"
#include "effect.h"
#include "utils.h"

cEffect_candle::cEffect_candle(Adafruit_NeoPixel *strip, pixel_t color1, pixel_t color2, int32_t sPix, int32_t ePix){
	sPixel=ePix>sPix?sPix:ePix;
	ePixel=ePix>sPix?ePix:sPix;
	pixel_c1 =color1;
	pixel_c2 =color2;
	neo_strip=strip;
	t_current_ms=0;
	SERIAL_PRINTLN("Start candle: " + String(sPix) + " -> " + String(ePix));
}

cEffect_candle::~cEffect_candle(){

}

int t_next=0;
bool cEffect_candle::loop(){
	pixel_t pix_c=pixel_c1;

	if (t_current_ms>t_next){
		//SERIAL_PRINTLN("Update: " + String(pix_c.h));
		t_current_ms=0;
		for (int32_t i=sPixel; i<ePixel;i++){
			pix_c.h=random(pixel_c1.h, pixel_c2.h);//(1000 * random(pixel_c1.h, pixel_c2.h))/1000;
			pix_c.s=random(pixel_c1.s, pixel_c2.s);//(1000 * random(pixel_c1.s, pixel_c2.s))/1000;
			pix_c.l=random(pixel_c1.l, pixel_c2.l);//(1000 * random(pixel_c1.l, pixel_c2.l))/1000;
			pix_c.w=random(pixel_c1.w, pixel_c2.w);//(1000 * random(pixel_c1.w, pixel_c2.w))/1000;
			neo_strip->setPixelColor(i,WRGB2COLOR(hsl2rgbw(pix_c.h, pix_c.s, pix_c.l, pix_c.w)));
		}
	} else {
		t_current_ms +=TICK_INT_MS;
		t_next=random(100);
	}
	return true;
}

bool cEffect_candle::stop(){
	return true;
}
