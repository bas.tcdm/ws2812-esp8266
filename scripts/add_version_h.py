import subprocess
label = subprocess.check_output(["C:\Program Files\Git\cmd\git", "describe", "--tags"]).strip().decode()
f = open("../version.h", "w")
f.write("#ifdef VARIANT_EFUE\n")
f.write("#  define VARIANT\t\"EFUE\"\n")
f.write("#endif\n\n")

f.write("#ifdef VARIANT_MICKEY\n")
f.write("#  define VARIANT\t\"MICKEY\"\n")
f.write("#endif\n\n")

f.write("#ifdef VARIANT_YINYANG\n")
f.write("#  define VARIANT\t\"YINYANG\"\n")
f.write("#endif\n\n")

f.write("#ifdef VARIANT_AQUARIUM\n")
f.write("#  define VARIANT\t\"AQUARIUM\"\n")
f.write("#endif\n\n")

f.write("#ifdef VARIANT_EMETER\n")
f.write("#  define VARIANT\t\"EMETER\"\n")
f.write("#endif\n\n")

f.write("#ifdef DEBUG\n")
f.write("#  define VERSION " + "\"" + str(label) + "_DEBUG\"\n")
f.write("#else\n")
f.write("#  define VERSION " + "\"" + str(label) + "\"\n")
f.write("#endif\n")
f.close()