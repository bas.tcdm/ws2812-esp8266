#ifndef EEPROM_H
#define EEPROM_H

//#include "cExtDevice.h"
#include "config.h"

void eeprom_init();
void eeprom_read_buf(uint32_t address, uint8_t *buf, uint32_t n);
void eepom_write_buf(uint32_t address, uint8_t *buf, uint32_t n);

#endif //EEPROM_H
