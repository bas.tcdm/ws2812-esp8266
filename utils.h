/*
 * utils.h
 *
 *  Created on: 19.11.2020
 *      Author: swielens
 */

#ifndef UTILS_H_
#define UTILS_H_

#include <stdint.h>
#include <stdlib.h>

uint32_t changeRGBbrightness(uint32_t color, uint8_t brightness);

void     hsl2rgbw(float H, float S, float L, float W, uint8_t* rgbw);
uint32_t hsl2rgbw(float H, float S, float L, float W);

int calculate_crc(uint8_t *buffer, uint32_t size);


#endif /* UTILS_H_ */
