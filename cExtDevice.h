/*
 * cExtDevice.h
 *
 *  Created on: 15.12.2020
 *      Author: swielens
 */

#ifndef C_EXT_DEVICE_H_
#define C_EXT_DEVICE_H_
#include "mqtt.h"


class cExtDevice {
public:
	String parent_topic;

	virtual void mqttMsgHandler(const String &topic, const String &payload){(void)topic;(void)payload;};
	virtual void reset_config(void){};
	virtual void loop_tick(int intervall_ms){(void)intervall_ms;};
	virtual void mqtt_connected(void){};
	virtual void mqtt_disconnected(void){};
	virtual void interrupt(){};
	virtual float get_energy(void){return 0.0;};
	virtual float get_power(void){return 0.0;};

	cExtDevice(){};
	virtual ~cExtDevice(){};
};


#endif /* C_EXT_DEVICE_H_ */
