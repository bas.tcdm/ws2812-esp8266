/*
 * utils.cpp
 *
 *  Created on: 19.11.2020
 *      Author: swielens
 */
#include "config.h"
#include "RGBConverter.h"
#include "cLedStripe.h"
#include "utils.h"

RGBConverter colorConverter;

/* Similar to above, but for an 8-bit gamma-correction table.
   Copy & paste this snippet into a Python REPL to regenerate:
import math
gamma=2.6
for x in range(256):
    print("{:3},".format(int(math.pow((x)/255.0,gamma)*255.0+0.5))),
    if x&15 == 15: print
*/
static const uint8_t PROGMEM _GammaCorrectionTable[256] = {
    0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
    0,  0,  0,  0,  0,  0,  0,  0,  1,  1,  1,  1,  1,  1,  1,  1,
    1,  1,  1,  1,  2,  2,  2,  2,  2,  2,  2,  2,  3,  3,  3,  3,
    3,  3,  4,  4,  4,  4,  5,  5,  5,  5,  5,  6,  6,  6,  6,  7,
    7,  7,  8,  8,  8,  9,  9,  9, 10, 10, 10, 11, 11, 11, 12, 12,
   13, 13, 13, 14, 14, 15, 15, 16, 16, 17, 17, 18, 18, 19, 19, 20,
   20, 21, 21, 22, 22, 23, 24, 24, 25, 25, 26, 27, 27, 28, 29, 29,
   30, 31, 31, 32, 33, 34, 34, 35, 36, 37, 38, 38, 39, 40, 41, 42,
   42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57,
   58, 59, 60, 61, 62, 63, 64, 65, 66, 68, 69, 70, 71, 72, 73, 75,
   76, 77, 78, 80, 81, 82, 84, 85, 86, 88, 89, 90, 92, 93, 94, 96,
   97, 99,100,102,103,105,106,108,109,111,112,114,115,117,119,120,
  122,124,125,127,129,130,132,134,136,137,139,141,143,145,146,148,
  150,152,154,156,158,160,162,164,166,168,170,172,174,176,178,180,
  182,184,186,188,191,193,195,197,199,202,204,206,209,211,213,215,
  218,220,223,225,227,230,232,235,237,240,242,245,247,250,252,255
};

uint32_t changeRGBbrightness(uint32_t color, uint8_t brightness){
  double hsl[3];
  uint8_t rgb[3];

  //convert to HSL
  uint32_t w = (color >> 24) & 0xFF;
  uint32_t b = color & 0xFF;
  uint32_t r = (color >> 16 ) & 0xFF;
  uint32_t g = (color >> 8) & 0xFF;

  rgb[0]=r;rgb[1]=g;rgb[2]=b;

  if ((color & 0xFFFFFF) != 0){
	  colorConverter.rgbToHsl(r,g,b, hsl);
	  //Change Brightness
	  double bd=brightness;
	  hsl[2]=bd/255;
	  //Convert back to rgb
	  colorConverter.hslToRgb(hsl[0], hsl[1], hsl[2], rgb);
  }

  double dw=brightness/255.0;
  w=(uint32_t)w*dw;

  //SERIAL_PRINTF("0x%X, 0x%X, 0x%X, 0x%X\n", w,r,g,b);
  color =  (w & 0xFF) << 24;
  color += (pgm_read_byte(&_GammaCorrectionTable[rgb[0]]) & 0xFF) << 16;
  color += (pgm_read_byte(&_GammaCorrectionTable[rgb[1]]) & 0xFF) << 8;
  color += (pgm_read_byte(&_GammaCorrectionTable[rgb[2]]) & 0xFF);

  return color;
}

uint32_t hsl2rgbw(float H, float S, float L, float W) {
	  float r, g, b;
	  float cos_h, cos_1047_h;
	  uint8_t rgbw[4];
	  H=H/1000;
	  S=S/1000;
	  L=L/1000;
	  W=W/1000;

	  //SERIAL_PRINTF("H=%f, S=%f, L=%f", H,S,L);

	  H = fmod(H,360); // cycle H around to 0-360 degrees
	  H = 3.14159*H/(float)180; // Convert to radians.
	  S = S>0?(S<1?S:1):0; // clamp S and L to interval [0,1]
	  L = L>0?(L<1?L:1):0;
	  W = W>0?(W<1?W:1):0;

     if(H < 2.09439) {
	    cos_h = cos(H);
	    cos_1047_h = cos(1.047196667-H);
	    r = S*255*L/3*(1+cos_h/cos_1047_h);
	    g = S*255*L/3*(1+(1-cos_h/cos_1047_h));
	    g=S>0.5?g+L*(S-0.5)*2*(255-g):g;
	    r=S>0.5?r+L*(S-0.5)*2*(255-r):r;

	    b=S>0.5?L*(S-0.5)*510:0;
	  } else if(H < 4.188787) {
	    H = H - 2.09439;
	    cos_h = cos(H);
	    cos_1047_h = cos(1.047196667-H);
	    g = S*255*L/3*(1+cos_h/cos_1047_h);
	    b = S*255*L/3*(1+(1-cos_h/cos_1047_h));
	    g=S>0.5?g+L*(S-0.5)*2*(255-g):g;
	    b=S>0.5?b+L*(S-0.5)*2*(255-b):b;

	    r=S>0.5?L*(S-0.5)*510:0;
	  } else {
	    H = H - 4.188787;
	    cos_h = cos(H);
	    cos_1047_h = cos(1.047196667-H);
	    b = S*255*L/3*(1+cos_h/cos_1047_h);
	    r = S*255*L/3*(1+(1-cos_h/cos_1047_h));
	    b=S>0.5?b+L*(S-0.5)*2*(255-b):b;
	    r=S>0.5?r+L*(S-0.5)*2*(255-r):r;

	    g=S>0.5?L*(S-0.5)*510:0;
	  }

      rgbw[0]=(int)b>255?255:b;
	  rgbw[1]=(int)g>255?255:g;
	  rgbw[2]=(int)r>255?255:r;
	  rgbw[3]=(int)(W*255);

	  rgbw[3]=pgm_read_byte(&_GammaCorrectionTable[rgbw[3]]);

	  //SERIAL_PRINTF(", R=%i, G=%i, B=%i\n",rgbw[2],rgbw[1],rgbw[0])

	  return rgbw[3] << 24 | (rgbw[2] << 16) | (rgbw[1] << 8) | (rgbw[0]);
}


/*uint32_t hsl2rgbw(float H, float S, float L, float W) {
	  float r, g, b;
	  float cos_h, cos_1047_h;
	  uint8_t rgbw[4];
	  H=H/1000;
	  S=S/1000;
	  L=L/1000;
	  W=W/1000;

	  SERIAL_PRINTF("H=%f, S=%f, L=%f", H,S,L);

	  H = fmod(H,360); // cycle H around to 0-360 degrees
	  H = 3.14159*H/(float)180; // Convert to radians.
	  S = S>0?(S<1?S:1):0; // clamp S and L to interval [0,1]
	  L = L>0?(L<1?L:1):0;
	  W = W>0?(W<1?W:1):0;

     if(H < 2.09439) {
	    cos_h = cos(H);
	    cos_1047_h = cos(1.047196667-H);
	    r = S*255*L/3*(1+cos_h/cos_1047_h);
	    g = S*255*L/3*(1+(1-cos_h/cos_1047_h));
	    b=0;
	  } else if(H < 4.188787) {
	    H = H - 2.09439;
	    cos_h = cos(H);
	    cos_1047_h = cos(1.047196667-H);
	    g = S*255*L/3*(1+cos_h/cos_1047_h);
	    b = S*255*L/3*(1+(1-cos_h/cos_1047_h));
	    r=0;
	  } else {
	    H = H - 4.188787;
	    cos_h = cos(H);
	    cos_1047_h = cos(1.047196667-H);
	    b = S*255*L/3*(1+cos_h/cos_1047_h);
	    r = S*255*L/3*(1+(1-cos_h/cos_1047_h));
	    g=0;
	  }

      //Compensate high saturation
      g=S>0.5?g+(L-0.5)*510:g;
      r=S>0.5?r+(L-0.5)*510:r;
      b=S>0.5?b+(L-0.5)*510:b;

      rgbw[0]=(int)b>255?255:b;
	  rgbw[1]=(int)g>255?255:g;
	  rgbw[2]=(int)r>255?255:r;
	  rgbw[3]=(int)(W*255);

	  rgbw[3]=pgm_read_byte(&_GammaCorrectionTable[rgbw[3]]);

	  SERIAL_PRINTF(", R=%i, G=%i, B=%i\n",rgbw[2],rgbw[1],rgbw[0])

	  return rgbw[3] << 24 | (rgbw[2] << 16) | (rgbw[1] << 8) | (rgbw[0]);
}*/


void hsl2rgbw(float H, float S, float L, float W, uint8_t* rgbw) {
	  int r, g, b;
	  float cos_h, cos_1047_h;

	  H=H/1000;
	  S=S/1000;
	  L=L/1000;
	  W=W/1000;

	  H = fmod(H,360); // cycle H around to 0-360 degrees
	  H = 3.14159*H/(float)180; // Convert to radians.
	  S = S>0?(S<1?S:1):0; // clamp S and L to interval [0,1]
	  L = L>0?(L<1?L:1):0;
	  W = W>0?(W<1?W:1):0;

	  if(H < 2.09439) {
	    cos_h = cos(H);
	    cos_1047_h = cos(1.047196667-H);
	    r = S*255*L/3*(1+cos_h/cos_1047_h);
	    g = S*255*L/3*(1+(1-cos_h/cos_1047_h));
	    b = 0;
	  } else if(H < 4.188787) {
	    H = H - 2.09439;
	    cos_h = cos(H);
	    cos_1047_h = cos(1.047196667-H);
	    g = S*255*L/3*(1+cos_h/cos_1047_h);
	    b = S*255*L/3*(1+(1-cos_h/cos_1047_h));
	    r = 0;
	  } else {
	    H = H - 4.188787;
	    cos_h = cos(H);
	    cos_1047_h = cos(1.047196667-H);
	    b = S*255*L/3*(1+cos_h/cos_1047_h);
	    r = S*255*L/3*(1+(1-cos_h/cos_1047_h));
	    g = 0;
	  }


	  rgbw[0]=b;
	  rgbw[1]=g;
	  rgbw[2]=r;
	  rgbw[3]=(int)(W*255);
}


int calculate_crc(uint8_t *buffer, uint32_t size)
{
  uint32_t crc = 0;
  for (uint32_t i = 0; i < size; i++) {
    crc ^= buffer[i]+1;
  }

  return crc;
}

