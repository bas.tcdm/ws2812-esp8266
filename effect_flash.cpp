/*
 * effect_flash.cpp
 *
 *  Created on: 23.11.2020
 *      Author: swielens
 */
#include "cLedStripe.h"
#include "effect.h"
#include "utils.h"

cEffect_flash::cEffect_flash(Adafruit_NeoPixel *strip, pixel_t *cur_c_buf, int32_t sPixel,int32_t ePixel,pixel_t c,	run_c_mode_t c_mode, int32_t ton_ms, int32_t toff_ms, int32_t rep){
	neo_strip=strip;

	start_pix=sPixel;
	end_pix=ePixel;
	color=c;
	color_mode=c_mode;
	t_on_ms=ton_ms;
	t_off_ms=toff_ms;
	repeat=rep;
	_cur_color_buf=cur_c_buf;

	_t_elapsed=0;
	_count=0;
	_stateOn=false;

}

cEffect_flash::~cEffect_flash(){}

bool cEffect_flash::loop(void){
	//SERIAL_PRINTF("Flash pattern time=%i: patterns[0].isOn=%i, patterns[0].int_cnt=%i\n",t_elapsed,patterns[0].isOn, patterns[0].int_cnt);
	//SERIAL_PRINTF("Flash pattern %i: t_elapsed \% patterns[i].ton_m=%i, patterns[i].isOn=%i, patterns[i].int_cnt=%i\n",i, t_elapsed % patterns[i].ton_ms, patterns[i].isOn, patterns[i].int_cnt);
	if (!_stateOn && (_t_elapsed % t_off_ms ==0)){
		//SERIAL_PRINTF("Switch on: %i ms, , cnt=%i \n", t_elapsed);
		for (int32_t p=start_pix;p<end_pix;p++){
			if (color_mode==RUN_MODE_COLOR) {
				uint32_t c = hsl2rgbw(color.h,color.s,color.l,color.w);
				neo_strip->setPixelColor(p, WRGB2COLOR(c));
			} else {
				uint32_t c = hsl2rgbw(_cur_color_buf[p].h,_cur_color_buf[p].s,color.l, _cur_color_buf[p].w);
				neo_strip->setPixelColor(p, WRGB2COLOR(c));
			}
		}
		_stateOn=true;
		//neo_strip->show();
	} else 	if (_stateOn && (_t_elapsed % t_on_ms ==0)){
		//SERIAL_PRINTF("Switch off: %i ms, count=%i\n", t_elapsed, patterns[i].int_cnt);
		for (int32_t p=start_pix;p<end_pix;p++){
			uint32_t c = hsl2rgbw(_cur_color_buf[p].h,_cur_color_buf[p].s,_cur_color_buf[p].l, _cur_color_buf[p].w);
			neo_strip->setPixelColor(p, WRGB2COLOR(c));
		}
		//neo_strip->show();
		_stateOn=false;
		_count++;
	}

	if (repeat!=0 && _count>=repeat) {
		//SERIAL_PRINTF("done, count=%i\n",_count);
		return false;
	}

	_t_elapsed+=TICK_INT_MS;
	//SERIAL_PRINTF("Time elapsed: %i\n",t_elapsed);
	return true;
}


