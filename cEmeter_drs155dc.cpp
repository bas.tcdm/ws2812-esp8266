/*
 * emeter_drs155dc.cpp
 *
 *  Created on: 15.12.2020
 *      Author: swielens
 */

#include "cEmeter.h"
#include "mqtt.h"
#include "utils.h"
#include "config.h"
#include "persistent_storage.h"

#define IRQ_TIMEOUT_MS			100
#define EEPROM_STORE_INT_SEC	86400 //Once per day
#define PUBLISH_INT_S			120 //Each 2 minutes

//static int	n_registered_emeters=0;
static cEmeter *registered_emeters[N_EMETERS];

ICACHE_RAM_ATTR void isr_callback(void *obj){
	cEmeter *meter=(cEmeter *)obj;
	if (meter!=NULL) meter->interrupt();
}

static void _mqttMsgHandler(const String &topic, const String &payload){
	SERIAL_PRINTLN(payload);
	for (int i=0; i< N_EMETERS; i++){
		if(registered_emeters[i]!=NULL){
			registered_emeters[i]->mqttMsgHandler(topic, payload);
		}
	}
}

cEmeter::cEmeter(unsigned int e_address, String p_topic, emeter_settings_t d_conf){
	pulse_cnt=0;
	pulse_cnt_last=0;
	memset((char*)&eeprom_config, 0, sizeof(eeprom_config));
	pulse_delta=0;
	power_kw=0.0;
	time_elapsed_ms=0;
	irq=false;
	irq_timeout=0;
	eeprom_address=e_address;
	tick_cnt=0;
	parent_topic=p_topic;

	eeprom_read_buf(eeprom_address, (unsigned char*)&eeprom_config, sizeof(eeprom_config));
	//SERIAL_PRINTF("INIT: CRC: %i, gpio=%i, pulses_per_kwh=%i, energy_kwh=%f\n", config.crc, config.gpio, config.pulses_per_kwh, config.energy_kwh);
	int crc=calculate_crc((unsigned char*)&eeprom_config.layout_version, sizeof(eeprom_config)-sizeof(int));

	if (crc!=eeprom_config.crc || eeprom_config.crc==0 || eeprom_config.layout_version!=EMETER_EEPROM_LAYOUT_VERSION){
		//Default values
		SERIAL_PRINTF("Wrong CRC: %i on eeprom_address=%i, eeprom_version=%i\n", crc,eeprom_address, eeprom_config.layout_version);
		eeprom_config.layout_version=EMETER_EEPROM_LAYOUT_VERSION;
		eeprom_config.energy_kwh=0.0;
		eeprom_config.settings=d_conf;

		String base_topic=String(eeprom_config.settings.mqtt_topic);
		base_topic.toCharArray((char*)&eeprom_config.settings.mqtt_topic, sizeof(eeprom_config.settings.mqtt_topic));
		eeprom_config.crc=calculate_crc((unsigned char*)&eeprom_config.layout_version, sizeof(eeprom_config)-sizeof(eeprom_config.crc));
		eepom_write_buf(e_address, (unsigned char*)&eeprom_config, sizeof(eeprom_config));

	}

	if (eeprom_config.settings.gpio >= 0){
		eeprom_config.energy_kwh=isnan(eeprom_config.energy_kwh)?0.0:eeprom_config.energy_kwh;
		pinMode(eeprom_config.settings.gpio, INPUT);
		attachInterruptArg(digitalPinToInterrupt(eeprom_config.settings.gpio), isr_callback, (void*)this, RISING);
		//interrupts();
	}
	SERIAL_PRINTF("cMeter Object on gpio=%i, pulses_per_kwh=%i, mqtt_base_topic=%s, e_address=%i, energy_kwh=%.3f, update_interval_s=%i\n", eeprom_config.settings.gpio,eeprom_config.settings.pulses_per_kwh,eeprom_config.settings.mqtt_topic, e_address,eeprom_config.energy_kwh, eeprom_config.settings.update_interval_s);
	String msg = "{";
	msg.concat("  \"gpio\": \""   + String(eeprom_config.settings.gpio));
	msg.concat(", \"pulses_per_kwh\": \""   + String(eeprom_config.settings.pulses_per_kwh));
	msg.concat(", \"mqtt_base_topic\": \""     + String(eeprom_config.settings.mqtt_topic) + "\"");
	msg.concat(", \"energy_kwh\": \""       + String(eeprom_config.energy_kwh));
	msg.concat("}");

	for (int i=0; i< N_EMETERS; i++){
		if(registered_emeters[i]==NULL){
			registered_emeters[i]=this;
			SERIAL_PRINTF("Add eMeter to index %i\n",i);
			break;
		}
	}
}

cEmeter::~cEmeter() {
	mqtt_unsubscribe(parent_topic + String(eeprom_config.settings.mqtt_topic) + String("/config"));
	mqtt_unsubscribe(parent_topic+ String(eeprom_config.settings.mqtt_topic) + String("/reset"));

	for (int i=0; i< N_EMETERS; i++){
		if(registered_emeters[i]==this){
			registered_emeters[i]=NULL;
			SERIAL_PRINTF("Removed eMeter from index %i\n",i);
		}
	}
}

void cEmeter::reset_config(){
	memset((char*)&eeprom_config, 0, sizeof(eeprom_config));
	eepom_write_buf(eeprom_address, (unsigned char*)&eeprom_config, sizeof(eeprom_config));
}

void cEmeter::mqtt_disconnected(void){
	mqtt_unsubscribe(parent_topic + String(eeprom_config.settings.mqtt_topic) + String("/config"));
	mqtt_unsubscribe(parent_topic+ String(eeprom_config.settings.mqtt_topic) + String("/reset"));
}

void cEmeter::mqtt_connected(void){
	mqtt_subscribe(parent_topic + String(eeprom_config.settings.mqtt_topic) + String("/config"), _mqttMsgHandler);
	mqtt_subscribe(parent_topic + String(eeprom_config.settings.mqtt_topic) + String("/reset"), _mqttMsgHandler);
}
void cEmeter::interrupt() {
	irq=true;
}

void cEmeter::emeter_publishResponse(String code, String msg) {
	if (!mqtt_isConnected()){
		SERIAL_PRINTLN("emeter_publishResponse: Lost Connection to Network");
		return;
	}
  //String topic=String(device_config.base_topic);
	mqtt_publish(parent_topic + String(eeprom_config.settings.mqtt_topic) + "/status", "{ \"code\": \"" + code + "\", \"msg\":\" " + msg + "\" }" );
}

void cEmeter::mqttMsgHandler(const String &topic, const String &payload){
	String mqtt_topic=String(eeprom_config.settings.mqtt_topic);
	//emeter_publishResponse("INFO", "Received publish: " + topic + ", " + payload);
	if (topic== String(parent_topic + mqtt_topic + "/config")){
		//emeter_publishResponse("INFO", "Received publish: " + topic + ", " + payload);
		SERIAL_PRINTLN("for me " + topic + " : \n" + payload);
		auto error = deserializeJson(jsonBuffer, payload);
		if (error) {
			SERIAL_PRINT(F("deserializeJson() failed with code "));
			SERIAL_PRINTLN(error.c_str());
			emeter_publishResponse("ERROR", "CONFIG: Payload missing or incorrect");
			return;
		}

		if (jsonBuffer.containsKey("update_interval_s")) {
			eeprom_config.settings.update_interval_s=jsonBuffer["update_interval_s"].as<int>();
		}

		if (jsonBuffer.containsKey("base_topic")) {
			mqtt_unsubscribe(String(eeprom_config.settings.mqtt_topic) + String("/config"));
			mqtt_unsubscribe(String(eeprom_config.settings.mqtt_topic) + String("/reset"));
			String topic=jsonBuffer["base_topic"].as<String>();
			if (!topic.startsWith("/")) {
				topic="/"+ topic;
			}
			topic.toCharArray(eeprom_config.settings.mqtt_topic, MAX_TOPIC_LENTH);
			mqtt_subscribe(String(eeprom_config.settings.mqtt_topic) + String("/config"), _mqttMsgHandler);
			mqtt_subscribe(String(eeprom_config.settings.mqtt_topic) + String("/reset"), _mqttMsgHandler);
		}

		eeprom_config.layout_version=EMETER_EEPROM_LAYOUT_VERSION;

		eeprom_config.crc=calculate_crc((unsigned char*)&eeprom_config.layout_version, sizeof(eeprom_config)-sizeof(int));
		SERIAL_PRINTF("store config on address=%i, CRC=%i\n", eeprom_address, eeprom_config.crc);
		eepom_write_buf(eeprom_address, (unsigned char*)&eeprom_config, sizeof(eeprom_config));

		detachInterrupt(digitalPinToInterrupt(eeprom_config.settings.gpio));
		pinMode(eeprom_config.settings.gpio, INPUT);
	    attachInterruptArg(digitalPinToInterrupt(eeprom_config.settings.gpio), isr_callback, (void*)this, RISING);

	    emeter_publishResponse("OK", "CONFIG updated: gpio=" + String(eeprom_config.settings.gpio) + " pulses_per_kwh=" + String(eeprom_config.settings.pulses_per_kwh) + " update_interval_s=" + String(eeprom_config.settings.update_interval_s) + " base_topic: " + String(eeprom_config.settings.mqtt_topic));
	    return;
	}

	if (topic== String(parent_topic + mqtt_topic + "/reset")) {
		//emeter_publishResponse("INFO", "Received publish: " + topic + ", " + payload);
		auto error = deserializeJson(jsonBuffer, payload);
		if (error) {
			SERIAL_PRINT(F("deserializeJson() failed with code "));
			SERIAL_PRINTLN(error.c_str());
			emeter_publishResponse("ERROR", "Reset: Payload missing or incorrect");
			return;
		}

		if (!jsonBuffer.containsKey("energy_kwh")) {
			emeter_publishResponse("ERROR", "Mandatory field energy_kwh missing");
			SERIAL_PRINTLN("ERROR: Mandatory field energy_kwh missing");
			return;
		} else {
			eeprom_config.energy_kwh=jsonBuffer["energy_kwh"].as<float>();
			eeprom_config.crc=calculate_crc((unsigned char*)&eeprom_config.layout_version, sizeof(eeprom_config)-sizeof(int));
			SERIAL_PRINTF("store config on address=%i, CRC=%i\n", eeprom_address, eeprom_config.crc);
			eepom_write_buf(eeprom_address, (unsigned char*)&eeprom_config, sizeof(eeprom_config));
			emeter_publishResponse("OK", "Reset energy to " +String(eeprom_config.energy_kwh,3));
		}
		return;
	}

	//emeter_publishResponse("INFO", "Received publish not for me: " + topic + ", " + payload);
	SERIAL_PRINTLN("Not for me: " + topic);
}

float cEmeter::get_energy(void){
	//SERIAL_PRINTF("E=%f\n",energy_kwh);
	return eeprom_config.energy_kwh;
}

float cEmeter::get_power(void){
	//SERIAL_PRINTF("P=%f\n",power_kw);
	return power_kw;
}


void cEmeter::loop_tick(int intervall_ms){
	time_elapsed_ms = time_elapsed_ms + intervall_ms;
	irq_timeout-=intervall_ms;
	tick_cnt++;

	if (irq && (irq_timeout<0)) {
		pulse_cnt++;
		//SERIAL_PRINTF("IRQ: %i, pulse_cnt=%i\n", default_config.gpio, pulse_cnt);
		irq=false;
		irq_timeout=IRQ_TIMEOUT_MS;
	} else {
		irq=false;
	}

	//SERIAL_PRINTF("Tick: update_interval_s=%i,time_elapsed_ms=%i\n ", config.update_interval_s, time_elapsed_ms)
	if ((time_elapsed_ms % (eeprom_config.settings.update_interval_s*1000)) == 0){
		pulse_delta=pulse_cnt -pulse_cnt_last;
		pulse_cnt_last=pulse_cnt;

		float e=pulse_delta;
		e=e/eeprom_config.settings.pulses_per_kwh;
		eeprom_config.energy_kwh+=e;

		power_kw=pulse_delta;
		power_kw=power_kw/eeprom_config.settings.pulses_per_kwh;
		power_kw=power_kw*3600/eeprom_config.settings.update_interval_s;

		String msg="{";
		msg.concat("\"mqtt_topic\": \"" + 	String(eeprom_config.settings.mqtt_topic) + "\"");
		msg.concat(", \"mqtt_parent\": \""     + parent_topic + "\"");
		msg.concat(", \"pulses_per_kwh\": " + 	String(eeprom_config.settings.pulses_per_kwh));
		msg.concat(", \"gpio\": " + 			String(eeprom_config.settings.gpio));
		msg.concat(", \"energy_kwh\": " + 		String(eeprom_config.energy_kwh,3));
		msg.concat(", \"power_kw\": " + 		String(power_kw,3));
		msg.concat(", \"update_interval_s\": " +String(eeprom_config.settings.update_interval_s));
		msg.concat("}");

		if (mqtt_isConnected()) {
			mqtt_publish(parent_topic + eeprom_config.settings.mqtt_topic,msg);
		} else {
			SERIAL_PRINTLN("cEmeter Lost connection")
		}

		//SERIAL_PRINTF("---\npulse_cnt=%i, pulse_cnt_last=%i, pulse_delta=%i, config.update_interval_s=%i time_elapsed_ms=%i, intervall_ms=%i, tick_cnt=%i\n",pulse_cnt, pulse_cnt_last, pulse_delta, config.update_interval_s, time_elapsed_ms/1000, intervall_ms,tick_cnt);
	}

	if (time_elapsed_ms>=EEPROM_STORE_INT_SEC*1000){
		SERIAL_PRINTF("Store E to EEPROM, time_elapsed_ms=%i\n",time_elapsed_ms);
		//Store energy inside EEPROM
		eeprom_config.crc=calculate_crc((unsigned char*)&eeprom_config.layout_version, sizeof(eeprom_config)-sizeof(int));
		eepom_write_buf(eeprom_address, (unsigned char*)&eeprom_config, sizeof( eeprom_config));
		time_elapsed_ms=0;
	}
}


