#ifndef _OTA_H_
#define _OTA_H_

void ota_setup(void);

void ota_loop(void);

#endif //_OTA_H_
