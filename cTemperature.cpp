/*
 * cDs18b2.cpp
 *
 *  Created on: 27.12.2021
 *      Author: swielens
 */

#include "cTemperature.h"
#include "mqtt.h"
#include "utils.h"
#include "config.h"
#include "persistent_storage.h"

#define READ_TEMP_INTERVALL_MS	5000

static cTemperature *registered_tsensors[N_TSENSORS];

static void _mqttMsgHandler(const String &topic, const String &payload){
	SERIAL_PRINTLN(payload);
	for (int i=0; i< N_TSENSORS; i++){
		if(registered_tsensors[i]!=NULL){
			registered_tsensors[i]->mqttMsgHandler(topic, payload);
		}
	}
}

cTemperature::cTemperature(unsigned int e_address, String p_topic, tsensor_settings_t d_conf){
	t_current=999;
	h_current=0;
	eeprom_address=e_address;
	parent_topic=p_topic;
	time_elapsed_ms=0;

	memset((char*)&eeprom_config, 0, sizeof(eeprom_config));

	eeprom_read_buf(eeprom_address, (unsigned char*)&eeprom_config, sizeof(eeprom_config));
	int crc=calculate_crc((unsigned char*)&eeprom_config.layout_version, sizeof(eeprom_config)-sizeof(int));
	if (crc!=eeprom_config.crc || eeprom_config.crc==0 || eeprom_config.layout_version!=TSENSOR_EEPROM_LAYOUT_VERSION){
		//Default values
		SERIAL_PRINTF("Wrong CRC: %i on eeprom_address=%i, eeprom_version=%i\n", crc,eeprom_address, eeprom_config.layout_version);
		eeprom_config.layout_version=TSENSOR_EEPROM_LAYOUT_VERSION;
		eeprom_config.settings=d_conf;

		String base_topic=String(eeprom_config.settings.mqtt_topic);
		base_topic.toCharArray((char*)&eeprom_config.settings.mqtt_topic, sizeof(eeprom_config.settings.mqtt_topic));

		eeprom_config.crc=calculate_crc((unsigned char*)&eeprom_config.layout_version, sizeof(eeprom_config)-sizeof(eeprom_config.crc));
		eepom_write_buf(e_address, (unsigned char*)&eeprom_config, sizeof(eeprom_config));
	}

	if (eeprom_config.settings.gpio >= 0){
		if (eeprom_config.settings.type==TSENSOR_DS18){
			SERIAL_PRINTF("New DS18 Sensor on gpio: %i\n",eeprom_config.settings.gpio);
			_oneWire=new OneWire(eeprom_config.settings.gpio);
			sensor=new DallasTemperature((OneWire*)_oneWire);
			((DallasTemperature*)sensor)->begin();
			((DallasTemperature*)sensor)->requestTemperatures();
			t_current=((DallasTemperature*)sensor)->getTempCByIndex(0);
		} else if (eeprom_config.settings.type==TSENSOR_DHT22){
			SERIAL_PRINTF("New DHT22 Sensor on gpio: %i\n",eeprom_config.settings.gpio);
			sensor= new DHT(eeprom_config.settings.gpio, DHT22);
			((DHT*)sensor)->begin();
			t_current=((DHT*)sensor)->readTemperature();
			h_current=((DHT*)sensor)->readHumidity();

		}
	} else {
		SERIAL_PRINTF("No Temperature Sensor connected");
	}

	for (int i=0; i< N_TSENSORS; i++){
		if(registered_tsensors[i]==NULL){
			registered_tsensors[i]=this;
			SERIAL_PRINTF("Add tsensor to index %i\n",i);
			break;
		}
	}

}

cTemperature::~cTemperature(){

}

void cTemperature::reset_config(){
	memset((char*)&eeprom_config, 0, sizeof(eeprom_config));
	eepom_write_buf(eeprom_address, (unsigned char*)&eeprom_config, sizeof(eeprom_config));
}

void cTemperature::mqtt_pubResponse(String code, String msg) {
	if (!mqtt_isConnected()){
		SERIAL_PRINTLN("tsensor_publishResponse: Lost Connection to Network");
		return;
	}
  //String topic=String(device_config.base_topic);
	mqtt_publish(parent_topic + String(eeprom_config.settings.mqtt_topic) + "/status", "{ \"code\": \"" + code + "\", \"msg\":\" " + msg + "\" }" );
}


void cTemperature::mqttMsgHandler(const String &topic, const String &payload){
	String mqtt_topic=String(eeprom_config.settings.mqtt_topic);
	SERIAL_PRINTLN(topic);
	//emeter_publishResponse("INFO", "Received publish: " + topic + ", " + payload);
	if (topic== String(parent_topic + mqtt_topic + "/config")){
		//emeter_publishResponse("INFO", "Received publish: " + topic + ", " + payload);
		SERIAL_PRINTLN("for me " + topic + " : \n" + payload);
		auto error = deserializeJson(jsonBuffer, payload);
		if (error) {
			SERIAL_PRINT(F("deserializeJson() failed with code "));
			SERIAL_PRINTLN(error.c_str());
			mqtt_pubResponse("ERROR", "CONFIG: Payload missing or incorrect");
			return;
		}

		if (jsonBuffer.containsKey("update_interval_s")) {
			eeprom_config.settings.update_interval_s=jsonBuffer["update_interval_s"].as<int>();
			SERIAL_PRINTF("Updated update_interval_s=%i\n", eeprom_config.settings.update_interval_s);
		}

		if (jsonBuffer.containsKey("base_topic")) {
			mqtt_unsubscribe(String(eeprom_config.settings.mqtt_topic) + String("/config"));
			String topic=jsonBuffer["base_topic"].as<String>();
			if (!topic.startsWith("/")) {
				topic="/"+ topic;
			}
		    topic.toCharArray(eeprom_config.settings.mqtt_topic, MAX_TOPIC_LENTH);
			mqtt_subscribe(String(eeprom_config.settings.mqtt_topic) + String("/config"), _mqttMsgHandler);
		}

		eeprom_config.layout_version=TSENSOR_EEPROM_LAYOUT_VERSION;

		eeprom_config.crc=calculate_crc((unsigned char*)&eeprom_config.layout_version, sizeof(eeprom_config)-sizeof(int));
		SERIAL_PRINTF("store config on address=%i, CRC=%i\n", eeprom_address, eeprom_config.crc);
		eepom_write_buf(eeprom_address, (unsigned char*)&eeprom_config, sizeof(eeprom_config));


		mqtt_pubResponse(String("OK"), String("CONFIG updated: gpio=") + String(eeprom_config.settings.gpio) + String(" update_interval_s=") + String(eeprom_config.settings.update_interval_s) + " base_topic: " + String(eeprom_config.settings.mqtt_topic));
	    return;
	}
}


void cTemperature::mqtt_disconnected(){
	mqtt_unsubscribe(parent_topic + String(eeprom_config.settings.mqtt_topic) + String("/config"));
}

void cTemperature::mqtt_connected(){
	mqtt_subscribe(parent_topic + String(eeprom_config.settings.mqtt_topic) + String("/config"), _mqttMsgHandler);
}

void cTemperature::loop_tick(int intervall_ms){
	if (sensor==NULL || time_elapsed_ms<eeprom_config.settings.update_interval_s*1000){
		time_elapsed_ms+=intervall_ms;
	}
	else {
		if (eeprom_config.settings.type==TSENSOR_DS18){
			((DallasTemperature*)sensor)->requestTemperatures();
			t_current=((DallasTemperature*)sensor)->getTempCByIndex(0);
		} else if (eeprom_config.settings.type==TSENSOR_DHT22){
			t_current=((DHT*)sensor)->readTemperature();
			h_current=((DHT*)sensor)->readHumidity();
		}
		time_elapsed_ms=0;
		//SERIAL_PRINTF("Curent Temperature: %4.2f �C\n", t_current);

		String msg="{";
		msg.concat("\"mqtt_topic\": \"" + 		String(eeprom_config.settings.mqtt_topic) + "\"");
		msg.concat(", \"mqtt_parent\": \"" + 	parent_topic + "\"");
		msg.concat(", \"temperature\": " + 		String(t_current));
		if (eeprom_config.settings.type==TSENSOR_DHT22){
			msg.concat(", \"humidity\": " + 		String(h_current));
		}
		msg.concat(", \"gpio\": " + 			String(eeprom_config.settings.gpio));
		msg.concat(", \"update_interval_s\": " +String(eeprom_config.settings.update_interval_s));
		msg.concat("}");

		if (mqtt_isConnected()) {
			mqtt_publish(parent_topic + eeprom_config.settings.mqtt_topic,msg);
		} else {
			SERIAL_PRINTLN("tSensor Lost connection to broker")
		}
	}
}

float cTemperature::get_temperature(){
	return t_current;
}

float cTemperature::get_humidity(void){
	return h_current;
}
