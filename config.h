#ifndef _YINYANG_H_
#define _YINYANG_H_

#include <Adafruit_NeoPixel.h>
#include <ArduinoOTA.h>
#include <ArduinoJson.h>

#define TICK_INT_MS			50
#define MAX_NUM_SEGMENTS 	6
#define MAX_SEEDS 			5

#define JSON_BUF_SIZE		1000
#define MAX_TOPIC_LENTH		80
#define MAX_VARIANT_LENTH   20

#define EEPROM_BASE_ADDRESS 0

//#define MQTT_SOCKET_TIMEOUT 30

typedef struct{
  int  	   h;
  int16_t  s;
  int16_t  l;
  int16_t  w;
}pixel_t;

/*
 * Variant configuration
 */
#ifdef VARIANT_EFUE
#  define MQTT_CLIENT_NAME  "eFue"
#  define N_RELAIS	0
#  define C_RELAIS  {}

#  define N_STRIPS	1
#  define C_STRIPS  {{/*gpio=*/4, /*n_pixels=*/195, /*s_config=*/STRIP_CONFIG, /*type=*/"SK6812",/*default_topic=*/"/light"}};
#  define SK6812

#  define N_EMETERS	0
#  define C_EMETERS {}
#  define POWER_ON_STATE 1
#elif defined(VARIANT_MICKEY)
#  define MQTT_CLIENT_NAME  "mickey"
#  define MQTT_CLIENT_SSID  "Virus-EG"
#  define MQTT_CLIENT_KEY   "sebastiaan.wielens"
#  define MQTT_CLIENT_GW    "192.168.1.2"
#  define MQTT_CLIENT_IP    "192.168.1.6"
#  define MQTT_CLIENT_MODE   MQTT_CLIENT

#  define N_RELAIS	0
#  define C_RELAIS  {}

#  define N_STRIPS	1
#  define C_STRIPS  {{/*gpio=*/4, /*n_pixels=*/150, /*s_config=*/STRIP_CONFIG, /*type=*/"SK6812",/*default_topic=*/"/light"}};
#  define SK6812

#  define N_EMETERS	0
#  define C_EMETERS {}

#  define N_TSENSORS	0
#  define C_TSENSORS {}

#  define POWER_ON_STATE 1
#elif defined(VARIANT_YINYANG)
#  define MQTT_CLIENT_NAME  "yinyang"
#  define MQTT_CLIENT_SSID  "Virus-EG"
#  define MQTT_CLIENT_KEY   "sebastiaan.wielens"
#  define MQTT_CLIENT_GW    "192.168.1.2"
#  define MQTT_CLIENT_IP    "192.168.1.6"
#  define MQTT_CLIENT_MODE   MQTT_CLIENT
#  define N_RELAIS	0
#  define C_RELAIS  {}

#  define N_STRIPS	1
#  define C_STRIPS  {{/*gpio=*/4, /*n_pixels=*/265, /*s_config=*/STRIP_CONFIG, /*type=*/"WS2812",/*default_topic=*/"/light"}};
#  define WS2812

#  define N_RELAIS	0
#  define C_RELAIS  {}

#  define N_EMETERS	0
#  define C_EMETERS {}

#  define N_TSENSORS	0
#  define C_TSENSORS {}

//#  define POWER_ON_STATE 1
#elif defined(VARIANT_EMETER)
#  define MQTT_CLIENT_NAME  "emeter"
#  define MQTT_CLIENT_SSID  "Virus-EG"
#  define MQTT_CLIENT_KEY   "sebastiaan.wielens"
#  define MQTT_CLIENT_GW    "192.168.1.2"
#  define MQTT_CLIENT_IP    "192.168.1.6"
#  define MQTT_CLIENT_MODE   MQTT_CLIENT

#  define N_RELAIS	0
#  define C_RELAIS  {}

#  define N_STRIPS	0
#  define C_STRIPS  {};

#  define N_EMETERS	3
#  define C_EMETERS {{/*gpio=*/12,/*pulses_per_kwh*/1000, /*update_interval_s*/120, /*base_topic*/"/phase0"},{/*gpio=*/13,/*pulses_per_kwh*/1000, /*update_interval_s*/120, /*base_topic*/"/phase1"},{/*gpio=*/14,/*pulses_per_kwh*/1000, /*update_interval_s*/120, /*base_topic*/"/phase2"}}

#  define N_TSENSORS	0
#  define C_TSENSORS {}

#  define USE_OLED
#  define OLED_HIGHT 64
#  define OLED_WIDTH 128
#  define OLED_I2C_ADDR	0x3C
//#  define POWER_ON_STATE 1
#elif defined(VARIANT_AQUARIUM)
#  define MQTT_CLIENT_NAME  "aquarium"
#  define N_RELAIS	4
#  define C_RELAIS  {{/*gpio*/14, /*device_topic*/"/w-pump"},{/*gpio*/13, /*device_topic*/"/a-pump"},{/*gpio*/12, /*device_topic*/"/uvc"},{/*gpio*/15, /*device_topic*/"/spare"}}

#  define N_STRIPS	1
#  define C_STRIPS  {{/*gpio=*/4, /*n_pixels=*/280, /*s_config=*/STRIP_CONFIG, /*type=*/"SK6812",/*default_topic=*/"/light"}};
#  define SK6812
#  define USE_OLED

#  define N_EMETERS	0
#  define C_EMETERS {}
#elif defined(VARIANT_LIGHT_WHEEL)
#  define MQTT_CLIENT_NAME  "ligth_wheel"
#  define MQTT_CLIENT_SSID  "Virus-EG"
#  define MQTT_CLIENT_KEY   "sebastiaan.wielens"
#  define MQTT_CLIENT_GW    "192.168.1.2"
#  define MQTT_CLIENT_IP    "192.168.1.6"
#  define MQTT_CLIENT_MODE   MQTT_CLIENT
#  define N_RELAIS	0
#  define C_RELAIS  {}

#  define N_STRIPS	1
#  define C_STRIPS  {{/*gpio=*/4, /*n_pixels=*/434, /*s_config=*/STRIP_CONFIG, /*type=*/"SK6812",/*default_topic=*/"/light"}};
#  define SK6812

#  define N_EMETERS	0
#  define C_EMETERS {}

#  define N_TSENSORS	0
#  define C_TSENSORS {}

#elif defined(VARIANT_GROW_BOX)
#  define MQTT_CLIENT_NAME  "grow_box"
#  define MQTT_CLIENT_SSID  "Virus-EG"
#  define MQTT_CLIENT_KEY   "sebastiaan.wielens"
#  define MQTT_CLIENT_GW    "192.168.1.2"
#  define MQTT_CLIENT_IP    "192.168.1.6"
#  define MQTT_CLIENT_MODE   MQTT_CLIENT

#  define N_RELAIS	3
#  define C_RELAIS  {{/*gpio*/14, /*invert*/ false, /*device_topic*/"/outside_fan"}, {/*gpio*/12, /*invert*/ false, /*device_topic*/"/light"}, {/*gpio*/16, /*invert*/ false, /*device_topic*/"/spare"}}

#  define N_STRIPS	0
#  define C_STRIPS  {};
#  define SK6812

#  define N_EMETERS	0
#  define C_EMETERS {}

#  define N_TSENSORS	2
#  define C_TSENSORS {{/*gpio*/0, /*type*/TSENSOR_DHT22, /*update_interval_s*/ 10, /*device_topic*/"/t-box"}, {/*gpio*/4, /*type*/TSENSOR_DHT22, /*update_interval_s*/ 10, /*device_topic*/"/t-room"}}
#else
#  define N_RELAIS	0
#  define N_STRIPS	0
#  define N_EMETERS	0
#  define N_TSENSORS 0
#  error "No variant specified"
#endif 

#ifdef DEBUG
#   define  SERIAL_BEGIN        Serial.begin(921600);delay(500);
#   define  SERIAL_PRINT(str)   Serial.print(str);
#   define  SERIAL_PRINTLN(str) Serial.println(str);
#   define  SERIAL_PRINTF(str, ...) Serial.printf(str,  ##__VA_ARGS__);
#   define  MQTT_LOG(level, msg)			mqtt_log(level, msg);
#else
#   define  SERIAL_BEGIN
#   define  SERIAL_PRINT(str)
#   define  SERIAL_PRINTLN(str)
#   define  SERIAL_PRINTF(str, ...)
#   define  MQTT_LOG(level, msg)
#endif

//#define _OLED
#ifdef USE_OLED


#else
#  define OLED_BEGIN()
#  define OLED_PRINTLN(str)
#  define OLED_PRINTBMP(bmp)
#endif

#define TOTAL_EXT_DEVICES (N_RELAIS + N_STRIPS + N_EMETERS + N_TSENSORS)


#endif //_YINYANG_H_
