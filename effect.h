/*
 * effect.h
 *
 *  Created on: 16.11.2020
 *      Author: swielens
 */

#ifndef EFFECT_H_
#define EFFECT_H_

#include <stdint.h>
#include <stdlib.h>

#include "cLedStripe.h"
#include "config.h"

class cEffect {

protected:
	void *strip;
	int32_t	step_ms, step_cnt;
	int32_t tick_int, tick_cnt,hit_cnt;

	int32_t t_current, t_end;

	Adafruit_NeoPixel *neo_strip;

public:
	virtual ~cEffect(){};
	virtual bool loop(void){return false;};
};

class cEffect_transition: public cEffect {
private:
	uint32_t t_trans_ms;
	uint32_t t_current_ms;

	pixel_t *pixels_step;
	pixel_t	*pixels_current;
	pixel_t	pixels_final;
	uint32_t	pixels_end;
	uint32_t	pixels_start;
public:
	~cEffect_transition();
	cEffect_transition(Adafruit_NeoPixel *strip,  uint32_t pStart, uint32_t pEnd, pixel_t *pixels_Cstart, pixel_t pixels_Cend, uint32_t transition_time_ms);
	bool loop(void);
};

/*
 * Effect wave
 */
#define MAX_PATTERN_SIZE	5
#define MAX_PATTERN_LEDS	300

typedef enum{
	RUN_MODE_COLOR=0x1,
	RUN_MODE_BRI=0x2
}run_c_mode_t;

typedef enum{
	RUN_TAIL_NEW=0x10,
	RUN_TAIL_OLD=0x20
}run_t_mode_t;

typedef enum {
	 RUN_DIR_LEFT,
	 RUN_DIR_RIGHT,
	 RUN_DIR_PINGPONG
 }run_dir_mode_e;;

class cEffect_run: public cEffect {
private:
	pixel_t 	*pixels;
	run_c_mode_t c_mode;
	run_t_mode_t t_mode;
	run_dir_mode_e	dir_mode,dir_mode_int;
	int32_t  	sPixel;
	int32_t  	ePixel;
	pixel_t  	pattern_buf[MAX_PATTERN_LEDS];
	int32_t  	p_size;
	int32_t  	step_ms;

	int32_t _t_elapsed;
	int32_t	_position;

	bool shiftRight();
	bool shiftLeft();
public:
	cEffect_run(Adafruit_NeoPixel *strip, pixel_t *pix,
			run_c_mode_t c_mode,
			run_t_mode_t t_mode,
			run_dir_mode_e  dir,
			int32_t  sPixel,
			int32_t  ePixel,
			pixel_t  *pBuf,
			int32_t  psize,
			int32_t  step_ms);
	~cEffect_run();

	bool loop(void);
};

/*
 * Flash effect
 */

class cEffect_flash: public cEffect {
private:
	int32_t			start_pix;
	int32_t			end_pix;
	pixel_t 		color;
	run_c_mode_t 	color_mode;
	int32_t 		t_on_ms;
	int32_t 		t_off_ms;
	int32_t			repeat;


	pixel_t 		*_cur_color_buf;
	int32_t			_t_elapsed;
	int32_t			_count;
	bool			_stateOn;
public:
	cEffect_flash(Adafruit_NeoPixel *strip, pixel_t *cur_color_buf, int32_t sPixel,int32_t ePixel,pixel_t c,	run_c_mode_t c_mode, int32_t ton_ms, int32_t toff_ms, int32_t rep);
	~cEffect_flash();
	//cEffect_flash(Adafruit_NeoPixel *strip, pattern_flash_t *patterns, int32_t np, int32_t tickTime_ms);
	bool loop(void);
};

class cEffect_candle: public cEffect {
private:
		int32_t		sPixel;
		int32_t		ePixel;
		pixel_t		pixel_c1;
		pixel_t		pixel_c2;
		int32_t		t_current_ms;
public:
		cEffect_candle(Adafruit_NeoPixel *strip, pixel_t color1, pixel_t color2, int32_t sPixel, int32_t ePixe);
		~cEffect_candle();
		bool loop(void);
		bool stop();
};

#endif /* EFFECT_H_ */
