/*
 * cTemperature.h
 *
 *  Created on: 27.12.2021
 *      Author: swielens
 */

#ifndef TEMPERATURE_H_
#define TEMPERATURE_H_

#include "cExtDevice.h"

#include <OneWire.h>
#include <DallasTemperature.h>
#include <DHT.h>

#define TSENSOR_EEPROM_LAYOUT_VERSION	3
#define TSENSOR_DS18	1
#define TSENSOR_DHT22	2

typedef struct {
	int gpio;
	int type;
	unsigned int update_interval_s;
	char mqtt_topic[MAX_TOPIC_LENTH];
}tsensor_settings_t;

typedef struct {
	int crc;
	int layout_version;
	tsensor_settings_t settings;
}tsensor_eeprom_t;

class cTemperature: public cExtDevice {
private:
	String 				parent_topic;
	tsensor_eeprom_t	eeprom_config;
	unsigned int 		eeprom_address;
	float				t_current;
	float				h_current;
	unsigned int 		time_elapsed_ms;

	void 	*_oneWire;
	void 	*sensor;

	//OneWire 			*_oneWire;
	//DallasTemperature 	*dallasT;

	StaticJsonDocument<150> jsonBuffer;

	void mqtt_pubResponse(String code, String msg);

public:
	cTemperature(unsigned int e_address, String p_topic, tsensor_settings_t d_conf);
	~cTemperature();

	void mqttMsgHandler(const String &topic, const String &payload);
	void mqtt_disconnected();
	void mqtt_connected();
	void loop_tick(int intervall_ms);
	void reset_config();

	float get_temperature(void);
	float get_humidity(void);
};


#endif /* TEMPERATURE_H_ */
