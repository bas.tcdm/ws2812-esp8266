//#include <ESP32HTTPUpdateServer.h>
#include "mqtt.h"
#include "eeprom.h"
#include "config.h"
#include <EspMQTTClient.h>

callback_Onconnection mqtt_client_Onconnection;

//MQTT Client
EspMQTTClient 	*mqtt_client;
String			client_base_topic;

void mqtt_client_publishStatus(String code, String msg) {
  mqtt_client->publish(client_base_topic, "{ \"code\": \"" + code + "\", \"msg\":\" " + msg + "\" }" );
}

void onConnectionEstablished() {
  SERIAL_PRINTLN("Started MQTT Client");
  if (mqtt_client_Onconnection!=NULL){
    mqtt_client_Onconnection();
  } else {
    SERIAL_PRINTF("No connection callback is set, forgot to call mqtt_setup?");
  }
}

void mqtt_client_setup(void (*callback)(void), client_config_t *client_config){
  mqtt_client_Onconnection=callback;
  client_base_topic=String(client_config->client_base_topic);
  SERIAL_PRINTLN("Start MQTT Client");
  mqtt_client= new EspMQTTClient(
    client_config->wlan_ssid,
    client_config->wlan_key,
    client_config->mqtt_broker_ip,
    client_config->client_name
  );
  //mqtt_client->enableDebuggingMessages(true);
  mqtt_client->setMaxPacketSize(1024);
  mqtt_client->setKeepAlive(10);
  mqtt_client->setMqttReconnectionAttemptDelay(2000);
  //mqtt_client->enableHTTPWebUpdater();
  WiFi.setSleepMode(WIFI_NONE_SLEEP); //WiFi.mode(WIFI_STA);
}

void mqtt_client_subscribe(const String &topic, void (*callback)(const String &topic, const String &payload)){
  mqtt_client->subscribe(topic, callback);
  SERIAL_PRINTLN("subscribe to " + topic);
}

void mqtt_client_unsubscribe(const String &topic){
  mqtt_client->unsubscribe(topic);
  SERIAL_PRINTLN("_unsubscribe to " + topic);
}

void mqtt_client_publish(const String &topic, const String &payload){
  mqtt_client->publish(topic, payload);
  SERIAL_PRINTLN("Publish to " + topic + ", Data " + payload);
}

bool mqtt_client_isConnected(void){
	return mqtt_client->isConnected();
}

void mqtt_client_loop(void){
  mqtt_client->loop();
}
