/*
 * cLedStripe.h
 *
 *  Created on: 19.12.2020
 *      Author: swielens
 */

#ifndef CLEDSTRIPE_H_
#define CLEDSTRIPE_H_

#include "effect.h"
#include "cExtDevice.h"

#define MAX_TRANS_EFFECTS		7
#define MAX_FLASH_EFFECTS		7
#define MAX_RUN_EFFECTS			7
#define MAX_CANDLE_EFFECTS		7

#define STRIP_EEPROM_LAYOUT_VERSION	4

#define LED_PIN    4
// How many NeoPixels are attached to the Arduino?
#define LED_COUNT 0

#if defined(SK6812)
#   define STRIP_CONFIG (NEO_RGBW + NEO_KHZ800)
#   define DT_LED_US		40								//r								//g						//b
#   define WRGB2COLOR(wrgb)  (wrgb & 0xFF000000)  | (((wrgb  >> 16) & 0xFF)<<8)  | (((wrgb  >> 8)  & 0xFF)<<16) | ((wrgb) & 0xFF)
#   define COLOR2WRGB(color) (color & 0xFF000000) | (((color >> 8)  & 0xFF)<<16) | (((color >> 16) & 0xFF)<<8) | ((color) & 0xFF)
#elif defined(WS2812)
#   define STRIP_CONFIG (NEO_GRB + NEO_KHZ800)
#   define DT_LED_US		15
#   define WRGB2COLOR(wrgb)	 ((wrgb & 0xFF0000)  | (wrgb & 0xFF00)  | (wrgb & 0xFF))
#   define COLOR2WRGB(color) ((color & 0xFF0000) | (color & 0xFF00) | (color & 0xFF))
#else
#   define STRIP_CONFIG (0)
#   define DT_LED_US		15
#   define WRGB2COLOR(wrgb)	 (wrgb & 0x0)
#   define COLOR2WRGB(color) (color & 0x0)
#endif

typedef struct {
  int sPixel;  //Start Pixel
  int ePixel;  //End pixel
  int        h;  //Color
  int16_t    s;  //Color
  int16_t    l;  //Color
  int16_t	 w;
} segment_t;

typedef struct {
	int gpio;
	int n_pixels;
	int s_config;
	char variant[MAX_VARIANT_LENTH];
	char mqtt_topic[MAX_TOPIC_LENTH];
}ledStrip_settings_t;

typedef struct {
	int    crc;
	int    layout_version;
	int32_t transition_ms;
	int32_t isOn;
	int 	n_segments;
	ledStrip_settings_t settings;
	segment_t segments[MAX_NUM_SEGMENTS];
}ledStripe_eeprom_t;

class cLedStripe: public cExtDevice {
private:
	ledStripe_eeprom_t		  c_eeprom;
	StaticJsonDocument<JSON_BUF_SIZE> jsonBuffer;
	int						e_address;

	pixel_t	    			*pixels;
	Adafruit_NeoPixel 		*strip;

	int time_elapsed_ms;

	void mqttCallback_config(const String &payload);
	void mqttCallback_set(const String &payload);
	void mqttCallback_run(const String &payload);
	void mqttCallback_flash(const String &payload);
	void mqttCallback_candle(const String &payload);
	void mqttCallback_seed(const String &payload);

	void publishResponse(String code, String msg);
	void publishState(void);
	void listSegements(void);
	void stripUpdate(void);

    //effects

    //Effect transitions
    cEffect *efects_transitions[MAX_TRANS_EFFECTS];
    uint32_t	n_transitions;
    void setTransition(uint32_t transition_ms);
    void transitions_reset();

    //effect wave
    cEffect_run 	*run_patterns[MAX_RUN_EFFECTS];
    uint32_t  		n_runs;
    void run_addPattern(pixel_t *color_buf, uint32_t buf_size, int32_t sPixel, int32_t ePixel, run_c_mode_t c_mode, run_t_mode_t t_mode, run_dir_mode_e dir, int32_t speed_ms);
    void run_reset();

    //effect flash
    cEffect_flash   *efects_flash[MAX_FLASH_EFFECTS];
    uint32_t  		n_flashes;
    void flash_addPattern(pixel_t color, int32_t sPixel, int32_t ePixel, run_c_mode_t c_mode, int32_t ton_ms, int32_t toff_ms, int32_t repeat_cnt);
    void flash_reset();

    cEffect_candle   *efects_candle[MAX_CANDLE_EFFECTS];
    uint32_t  		n_candles;
    void candle_add(pixel_t color1, pixel_t color2, int32_t sPixel, int32_t ePixel);
    void candle_reset();


public:
    ~cLedStripe();
	cLedStripe(unsigned int e_address, String base_topic, ledStrip_settings_t default_conf);
	void loop_tick(int intervall_ms);
	void reset_config();
	void mqttMsgHandler(const String &topic, const String &payload);
	void mqtt_connected(void);
	void mqtt_disconnected(void);
	String get_base_topic();
};



#endif /* CLEDSTRIPE_H_ */
