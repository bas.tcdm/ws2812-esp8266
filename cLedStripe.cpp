/*
 * cLedStripe.cpp
 *
 *  Created on: 19.12.2020
 *      Author: swielens
 */

#include "cLedStripe.h"
#include "utils.h"
#include "persistent_storage.h"

#define RELAIS_PUBLISH_INT_S			120 //Each 2 minutes

//static int	n_registered_strips=0;
static cLedStripe *registered_strips[N_STRIPS];

static void _mqttMsgHandler(const String &topic, const String &payload){
	SERIAL_PRINTLN(payload);
	for (int i=0; i< N_STRIPS; i++){
		if(registered_strips[i]!=NULL){
			registered_strips[i]->mqttMsgHandler(topic, payload);
		}
	}
}

cLedStripe::cLedStripe(unsigned int ee_address, String base_topic, ledStrip_settings_t default_conf){
	e_address=ee_address;
	n_transitions=0;
	n_runs=0;
	n_flashes=0;
	n_candles=0;
	time_elapsed_ms=0;

	eeprom_read_buf(e_address, (unsigned char*)&c_eeprom,  sizeof(ledStripe_eeprom_t));
	SERIAL_PRINTF("INIT: CRC: %i, gpio=%i, n_leds=%i\n", c_eeprom.crc, c_eeprom.settings.gpio, c_eeprom.settings.n_pixels);

	parent_topic=base_topic;

	int crc=calculate_crc((unsigned char*)&c_eeprom.layout_version, sizeof(ledStripe_eeprom_t)-sizeof(int));
	if (crc!=c_eeprom.crc || c_eeprom.crc==0 || c_eeprom.layout_version!=STRIP_EEPROM_LAYOUT_VERSION || c_eeprom.settings.gpio>10){
		//Default settings
		memset(&c_eeprom, 0, sizeof(c_eeprom));
		c_eeprom.layout_version=STRIP_EEPROM_LAYOUT_VERSION;
		c_eeprom.n_segments=0;
		c_eeprom.settings=default_conf;
		c_eeprom.crc=calculate_crc((unsigned char*)&c_eeprom.layout_version, sizeof(c_eeprom)-sizeof(c_eeprom.crc));
		eepom_write_buf(e_address, (unsigned char*)&c_eeprom, sizeof(c_eeprom));
	}

	pixels=(pixel_t*)malloc(c_eeprom.settings.n_pixels*sizeof(pixel_t));
	if (pixels==NULL) {
		SERIAL_PRINTF("Could not create pixel memory: %i\n", strip->numPixels());
	}

	if (c_eeprom.settings.n_pixels>0 && c_eeprom.settings.gpio>=0){
		strip = new Adafruit_NeoPixel(c_eeprom.settings.n_pixels, c_eeprom.settings.gpio,  c_eeprom.settings.s_config);
		strip->begin();
		strip->setBrightness(255);
		SERIAL_PRINTF("Create new neopixel instance=0x%X, leds=%i\n", (int)strip,strip->numPixels());

		for (int32_t i = 0; i < c_eeprom.n_segments; i++) {
			//SERIAL_PRINTF("i %i\n", i);
			//config.n_segments++;
#ifdef POWER_ON_STATE
			c_eeprom.isOn=POWER_ON_STATE;
#endif
			c_eeprom.segments[i].sPixel=c_eeprom.segments[i].sPixel<c_eeprom.settings.n_pixels?c_eeprom.segments[i].sPixel:c_eeprom.settings.n_pixels;
			c_eeprom.segments[i].ePixel=c_eeprom.segments[i].ePixel<c_eeprom.settings.n_pixels?c_eeprom.segments[i].ePixel:c_eeprom.settings.n_pixels;
			SERIAL_PRINTF("Read segment %i, on: %i, Color (%i,%i,%i), Start: %i, End: %i\n", i, c_eeprom.isOn, c_eeprom.segments[i].h, c_eeprom.segments[i].s,c_eeprom.segments[i].l,c_eeprom.segments[i].sPixel,c_eeprom.segments[i].ePixel);
			for (int p=c_eeprom.segments[i].sPixel; p <= c_eeprom.segments[i].ePixel; p++ ){
				pixels[p].h=0;
				pixels[p].s=0;
				pixels[p].l=0;
				pixels[p].w=0;
			}
		}
		//SERIAL_PRINTLN("cLedStripe::cLedStripe 4");
		stripUpdate();
	}

	for (int i=0; i< N_STRIPS; i++){
		if(registered_strips[i]==NULL){
			registered_strips[i]=this;
			SERIAL_PRINTF("Add Strip to index %i\n",i);
			break;
		}
	}
}

cLedStripe::~cLedStripe() {
	mqtt_unsubscribe(parent_topic + String(c_eeprom.settings.mqtt_topic) + String("/config"));
	mqtt_unsubscribe(parent_topic + String(c_eeprom.settings.mqtt_topic) + String("/set"));
	mqtt_unsubscribe(parent_topic + String(c_eeprom.settings.mqtt_topic) + String("/flash"));
	mqtt_unsubscribe(parent_topic + String(c_eeprom.settings.mqtt_topic) + String("/seed"));
	mqtt_unsubscribe(parent_topic + String(c_eeprom.settings.mqtt_topic) + String("/run"));
	mqtt_unsubscribe(parent_topic + String(c_eeprom.settings.mqtt_topic) + String("/candle"));

	for (uint32_t i=0; i< n_runs; i++){
		if (run_patterns[i]!=NULL) delete run_patterns[i];
	}

	for (uint32_t i=0; i< n_flashes; i++){
		if (efects_flash[i]!=NULL) delete efects_flash[i];
	}

	for (uint32_t i=0; i< n_transitions; i++){
		if (efects_transitions[i]!=NULL) delete efects_transitions[i];
	}

	for (int i=0; i< N_STRIPS; i++){
		if(registered_strips[i]==this){
			registered_strips[i]=NULL;
			SERIAL_PRINTF("Removed Strip from index %i\n",i);
		}
	}
	 delete strip;
}

void cLedStripe::reset_config(){
	memset((char*)&c_eeprom, 0, sizeof(c_eeprom));
	eepom_write_buf(e_address, (unsigned char*)&c_eeprom, sizeof(c_eeprom));
}

void cLedStripe::mqtt_disconnected(void){
	mqtt_unsubscribe(parent_topic + String(c_eeprom.settings.mqtt_topic) + String("/config"));
	mqtt_unsubscribe(parent_topic + String(c_eeprom.settings.mqtt_topic) + String("/set"));
	mqtt_unsubscribe(parent_topic + String(c_eeprom.settings.mqtt_topic) + String("/flash"));
	mqtt_unsubscribe(parent_topic + String(c_eeprom.settings.mqtt_topic) + String("/seed"));
	mqtt_unsubscribe(parent_topic + String(c_eeprom.settings.mqtt_topic) + String("/run"));
	mqtt_unsubscribe(parent_topic + String(c_eeprom.settings.mqtt_topic) + String("/candle"));
}

void cLedStripe::mqtt_connected(void){
	mqtt_subscribe(parent_topic + String(c_eeprom.settings.mqtt_topic) + String("/config"), _mqttMsgHandler);
	mqtt_subscribe(parent_topic + String(c_eeprom.settings.mqtt_topic) + String("/set"),    _mqttMsgHandler);
	mqtt_subscribe(parent_topic + String(c_eeprom.settings.mqtt_topic) + String("/flash"),  _mqttMsgHandler);
	mqtt_subscribe(parent_topic + String(c_eeprom.settings.mqtt_topic) + String("/seed"),   _mqttMsgHandler);
	mqtt_subscribe(parent_topic + String(c_eeprom.settings.mqtt_topic) + String("/run"),   _mqttMsgHandler);
	mqtt_subscribe(parent_topic + String(c_eeprom.settings.mqtt_topic) + String("/candle"),   _mqttMsgHandler);
}

void cLedStripe::mqttMsgHandler(const String &topic, const String &payload){
	//SERIAL_PRINT("cLedStripe::mqttMsgHandler " + topic);
	//SERIAL_PRINTLN(",Payload:" + payload);
	String mqtt_topic=parent_topic + String(c_eeprom.settings.mqtt_topic);
	if (topic== String(mqtt_topic + "/config")){
		mqttCallback_config(payload);
	} else if (topic== String(mqtt_topic + "/set")){
		mqttCallback_set(payload);
	} else if (topic== String(mqtt_topic + "/run")){
		mqttCallback_run(payload);
	} else if (topic== String(mqtt_topic + "/flash")){
		mqttCallback_flash(payload);
	} else if (topic== String(mqtt_topic + "/candle")){
		mqttCallback_candle(payload);
	}
}

void cLedStripe::publishResponse(String code, String msg) {
  //String topic=String(device_config.base_topic);
	mqtt_publish(parent_topic + String(c_eeprom.settings.mqtt_topic) + "/response", "{ \"code\": \"" + code + "\", \"msg\":\" " + msg + "\" }" );
}

void cLedStripe::publishState() {
  String msg = "{";
  msg.concat("  \"state\": \""         	 + String(c_eeprom.isOn?"ON":"OFF") + "\"");
  msg.concat(", \"transition\": "      	 + String(c_eeprom.transition_ms));
  msg.concat(", \"mqtt_topic\": \""      + String(c_eeprom.settings.mqtt_topic) + "\"");
  msg.concat(", \"mqtt_parent\": \""     + parent_topic + "\"");
  msg.concat(", \"segments\":"         	 + String(c_eeprom.n_segments));
  msg.concat(", \"max_segments\": "    	 + String(MAX_NUM_SEGMENTS));
  msg.concat(", \"leds\": "            	 + String(strip->numPixels()));
  msg.concat(", \"gpio\": "            	 + String(c_eeprom.settings.gpio));
  msg.concat(", \"strip\": \""		   	+ String(c_eeprom.settings.variant) + "\"");
  msg.concat("}");
  mqtt_publish(parent_topic + String(c_eeprom.settings.mqtt_topic), msg);
}

/***************************************************
{
  "nLeds"         : n,
  "client_name"   : "",
  "wlan_ssid"     : "",
  "wlan_key"      : "",
  "wlan_gateway"  : "",
  "mqtt_broker_ip": "",
  "base_topic"    : "",
  "mqtt_mode"     : "MQTT_CLIENT" or "MQTT_BROKER"
}
 ****************************************************/
void cLedStripe::mqttCallback_config(const String &payload) {
	SERIAL_PRINTLN("mqttCallback_config: " + payload);
	bool isOk=false;
	auto error = deserializeJson(jsonBuffer, payload);
	if (error) {
		SERIAL_PRINT(F("deserializeJson() failed with code "));
		SERIAL_PRINTLN(error.c_str());
		publishResponse("ERROR", "SET: Payload missing or incorrect");
		return;
	}

	if (jsonBuffer.containsKey("mqtt_topic")) {
		String topic=jsonBuffer["mqtt_topic"].as<String>();
		if (!topic.startsWith("/")) {
			topic="/"+ topic;
		}
		if (topic.length()<MAX_TOPIC_LENTH) {
			mqtt_disconnected();
			topic.toCharArray((char*)&c_eeprom.settings.mqtt_topic,MAX_TOPIC_LENTH);
			SERIAL_PRINTF("Configure base_topic: %s\n", c_eeprom.settings.mqtt_topic);
			isOk &= isOk;
		} else {
			SERIAL_PRINTLN("ERROR: Topic length exceeds maximum " + String(MAX_TOPIC_LENTH));
			publishResponse("ERROR", "Topic length exceeds maximum " + String(MAX_TOPIC_LENTH));
			isOk=false;
		}
	} else {
		isOk=false;
	}
	c_eeprom.crc=calculate_crc((unsigned char*)&c_eeprom.layout_version, sizeof(ledStripe_eeprom_t)-sizeof(int));
	eepom_write_buf(e_address, (unsigned char*)&c_eeprom, sizeof(c_eeprom));
	//ESP.reset();

	delete strip;
	strip = new Adafruit_NeoPixel(c_eeprom.settings.n_pixels, c_eeprom.settings.n_pixels,  c_eeprom.settings.s_config);
	if (isOk && strip!=NULL){
		strip->begin();
		strip->setBrightness(255);
		c_eeprom.crc=calculate_crc((unsigned char*)&c_eeprom.layout_version, sizeof(ledStripe_eeprom_t)-sizeof(int));
		eepom_write_buf(e_address, (unsigned char*)&c_eeprom, sizeof(c_eeprom));

		mqtt_connected();

		publishResponse("OK", "LED config");
	} else {
		SERIAL_PRINTF("Something went wrong: isOk=%i, strip=0x%X\n", isOk, (unsigned int)strip);
	}
}

/*
 * Effect RUN
 {
	"t_mode": "RUN_TAIL_NEW",
	"c_mode": "RUN_MODE_COLOR",
	"sPixel": 0,
    "ePixel": 100,
	"step_ms": 20,
	"pattern": "[
		'{ \"wrgb\": \"0xffff\",   \"brightness\": 20}',
		'{ \"wrgb\": \"0xffff\",   \"brightness\": 40}',
		'{ \"wrgb\": \"0xffff\",   \"brightness\": 70}',
		'{ \"wrgb\": \"0xffff\",   \"brightness\": 100}',
		'{ \"wrgb\": \"0xffff\",   \"brightness\": 150}',
		'{ \"wrgb\": \"0xffff\",   \"brightness\": 120}'
	]"
}
 */
void cLedStripe::mqttCallback_run(const String &payload) {
	//SERIAL_PRINTLN("mqttCallback_RUN: "+ payload);
	if (payload.length()>JSON_BUF_SIZE-2){
		publishResponse("ERROR", "RUN: Payload lenth to big: " + String(payload.length()));
		return;
	}

	auto error = deserializeJson(jsonBuffer, payload);
	if (error) {
		publishResponse("ERROR", "RUN: Payload missing or incorrect: " + String(error.c_str()));
		return;
	}

	if (c_eeprom.settings.n_pixels==0 || c_eeprom.settings.gpio==-1) {
		publishResponse("ERROR", "Strip not configured correctly: n_leds=" + String(c_eeprom.settings.n_pixels) + ", gpio= " + String(c_eeprom.settings.gpio));
		return;
	}

	  //Mandatory field state
	if (jsonBuffer.containsKey("state")) {
		if (jsonBuffer["state"].as<String>()=="OFF") {
			run_reset();
			publishResponse("OK", "Stopped RUN");
			return;
		}
	}

	int32_t step_ms=0;
	if (jsonBuffer.containsKey("step_ms")) {
		step_ms=jsonBuffer["step_ms"].as<int>();
	} else {
		publishResponse("ERROR", "RUN: field step_ms missing");
		return;
	}

	int32_t sPixel=0;
	if (jsonBuffer.containsKey("sPixel")) {
		sPixel=jsonBuffer["sPixel"].as<int>();
		//SERIAL_PRINTF("sPixel: %i\n", sPixel);
	} else {
		publishResponse("ERROR", "RUN: field sPixel missing");
		return;
	}

	int32_t ePixel=0;
	if (jsonBuffer.containsKey("ePixel")) {
		ePixel=jsonBuffer["ePixel"].as<int>();
		//SERIAL_PRINTF("ePixel: %i\n", ePixel);
	} else {
		publishResponse("ERROR", "RUN: field ePixel missing");
		return;
	}

	run_c_mode_t c_mode=RUN_MODE_COLOR;
	if (jsonBuffer.containsKey("c_mode")) {
		c_mode=jsonBuffer["c_mode"].as<String>()=="RUN_MODE_COLOR"?RUN_MODE_COLOR:RUN_MODE_BRI;
	} else {
		publishResponse("ERROR", "RUN: field c_mode missing");
		return;
	}

	run_t_mode_t t_mode=RUN_TAIL_OLD;
	if (jsonBuffer.containsKey("t_mode")) {
		t_mode=jsonBuffer["t_mode"].as<String>()=="RUN_TAIL_OLD"?RUN_TAIL_OLD:RUN_TAIL_NEW;
	} else {
		publishResponse("ERROR", "RUN: field t_mode missing");
		return;
	}

	run_dir_mode_e dir=RUN_DIR_RIGHT;
	if (jsonBuffer.containsKey("dir")) {
		dir=jsonBuffer["dir"].as<String>()=="RUN_DIR_RIGHT"?RUN_DIR_RIGHT:RUN_DIR_RIGHT;
		dir=jsonBuffer["dir"].as<String>()=="RUN_DIR_LEFT"?RUN_DIR_LEFT:RUN_DIR_RIGHT;
		dir=jsonBuffer["dir"].as<String>()=="RUN_DIR_PINGPONG"?RUN_DIR_PINGPONG:RUN_DIR_RIGHT;
	}

	int32_t pSize=0;
	pixel_t *pattern=NULL;
	if (jsonBuffer.containsKey("pattern")) {
	  String str_pat=jsonBuffer["pattern"];
	  auto error = deserializeJson(jsonBuffer, str_pat);
	  if (error) {
		publishResponse("ERROR", "RUN: pattern field incorrect: " + String(error.c_str()));
	  } else {
		// extract the values
		//strip->run_reset();
		JsonArray array = jsonBuffer.as<JsonArray>();
		pSize=array.size();
		pattern=(pixel_t*)malloc(pSize*sizeof(pixel_t));
		if (pattern==NULL) {
			publishResponse("ERROR", "Failed to reserve pattern memory");
			run_reset();
			return;
		}
		//SERIAL_PRINTF("Found pattern of length %i\n", array.size());
		int32_t i=0;
		for(JsonVariant v : array) {
			StaticJsonDocument<200> json_pattern;
			String element=v.as<String>();
			//SERIAL_PRINTLN(element);
			deserializeJson(json_pattern, element);

			if (json_pattern.containsKey("h")) {
				pattern[i].h=1000*json_pattern["h"].as<int>();
			} else {
				publishResponse("ERROR", "Missing h field");
				run_reset();
				return;
			}

			if (json_pattern.containsKey("s")) {
				float s=1000*json_pattern["s"].as<float>();
				pattern[i].s=(int)s;
			} else {
				publishResponse("ERROR", "Missing s field");
				run_reset();
				return;
			}

			if (json_pattern.containsKey("l")) {
				float l=1000*json_pattern["l"].as<float>();
				pattern[i].l=(int)l;
			} else {
				publishResponse("ERROR", "Missing l field");
				run_reset();
				return;
			}

			if (json_pattern.containsKey("w")) {
				float w=1000*json_pattern["w"].as<float>();
				pattern[i].w=(int)w;
			} else {
				pattern[i].w=0;
			}
			i++;
		}
	  }
	}

	if (pattern!=NULL){
		run_addPattern(pattern, pSize, sPixel, ePixel, c_mode, t_mode, dir, step_ms);
		//strip->RUN_start(step_ms);
		free(pattern);
		publishResponse("OK", "RUN triggered");
	}
}

/*
{
	"segments": "[ '{
			\"wrgb\": \"0xFF00\",
			\"brightness\": 100,
			\"c_mode\": \"RUN_MODE_COLOR\",
			\"sPixel\": 1,
			\"ePixel\": 3,
			\"ton_ms\": 100,
			\"toff_ms\": 200,
			\"repeat_cnt\": 5
		}',
		'{
			\"wrgb\": \"0xFF0000\",
			\"brightness\": 100,
			\"c_mode\": \"RUN_MODE_COLOR\",
			\"sPixel\": 4,
			\"ePixel\": 7,
			\"ton_ms\": 10,
			\"toff_ms\": 200,
			\"repeat_cnt\": 5
		}',
		'{
			\"wrgb\": \"0xFF\",
			\"brightness\": 100,
			\"c_mode\": \"RUN_MODE_COLOR\",
			\"sPixel\": 8,
			\"ePixel\": 10,
			\"ton_ms\": 500,
			\"toff_ms\": 500,
			\"repeat_cnt\": 10
		}'
	]"
}
 */
void cLedStripe::mqttCallback_flash(const String &payload) {
	SERIAL_PRINTLN("mqttCallback_flash: "+ payload);
	if (payload.length()>JSON_BUF_SIZE-2){
		publishResponse("ERROR", "RUN: Payload lenth to big: " + String(payload.length()));
		return;
	}

	auto error = deserializeJson(jsonBuffer, payload);
	if (error) {
		SERIAL_PRINT(F("deserializeJson() failed with code "));
		SERIAL_PRINTLN(error.c_str());
		publishResponse("ERROR", "RUN: Payload missing or incorrect: " + String(error.c_str()));
		return;
	}

	if (c_eeprom.settings.n_pixels==0 || c_eeprom.settings.gpio==-1) {
		publishResponse("ERROR", "Strip not configured correctly: n_leds=" + String(c_eeprom.settings.n_pixels) + ", gpio= " + String(c_eeprom.settings.gpio));
		return;
	}

	if (jsonBuffer.containsKey("state")) {
		if (jsonBuffer["state"].as<String>()=="OFF") {
			flash_reset();
			publishResponse("OK", "Stopped flash");
			return;
		}
	}

	if (jsonBuffer.containsKey("segments")) {
	  String str_pat=jsonBuffer["segments"];
	  //SERIAL_PRINTLN("pattern: " + str_pat);
	  auto error = deserializeJson(jsonBuffer, str_pat);
	  if (error) {
		publishResponse("ERROR", "Flash: pattern field incorrect: " + String(error.c_str()));
	  } else {
		// extract the values
		flash_reset();
		JsonArray array = jsonBuffer.as<JsonArray>();
		//SERIAL_PRINTF("Found pattern of length %i\n", array.size());
		for(JsonVariant v : array) {
			StaticJsonDocument<1000> json_pattern;
			String element=v.as<String>();
			//SERIAL_PRINTLN(element);
			deserializeJson(json_pattern, element);

			float h=0,s=0,l=0,w=0;
			if (json_pattern.containsKey("h")) {
				h=1000*json_pattern["h"].as<int>();
			} else {
				publishResponse("ERROR", "Missing h field");
				flash_reset();
				return;
			}

			if (json_pattern.containsKey("s")) {
				s=json_pattern["s"].as<float>();
			} else {
				publishResponse("ERROR", "Missing s field");
				flash_reset();
				return;
			}

			if (json_pattern.containsKey("l")) {
				l=json_pattern["l"].as<float>();
			} else {
				publishResponse("ERROR", "Missing l field");
				flash_reset();
				return;
			}

			if (json_pattern.containsKey("w")) {
				w=json_pattern["w"].as<float>();
			} else {
				w=0;
			}

			run_c_mode_t c_mode=RUN_MODE_COLOR;
			if (json_pattern.containsKey("c_mode")){
				c_mode=(json_pattern["c_mode"].as<String>()=="RUN_MODE_COLOR")?RUN_MODE_COLOR:RUN_MODE_BRI;
			} else {
				publishResponse("ERROR", "Missing c_mode field");
				flash_reset();
				return;
			}

			int32_t sPixel=0;
			if (json_pattern.containsKey("sPixel")){
				sPixel =json_pattern["sPixel"].as<int>();
			} else {
				publishResponse("ERROR", "Missing sPixel field");
				flash_reset();
				return;
			}

			int32_t ePixel=0;
			if (json_pattern.containsKey("ePixel")){
				ePixel =json_pattern["ePixel"].as<int>();
			} else {
				publishResponse("ERROR", "Missing ePixel field");
				flash_reset();
				return;
			}

			int32_t ton_ms=0;
			if (json_pattern.containsKey("ton_ms")){
				ton_ms =json_pattern["ton_ms"].as<int>();
			} else {
				publishResponse("ERROR", "Missing ton_ms field");
				flash_reset();
				return;
			}

			int32_t toff_ms=0;
			if (json_pattern.containsKey("toff_ms")){
				toff_ms =json_pattern["toff_ms"].as<int>();
			} else {
				publishResponse("ERROR", "Missing toff_ms field");
				flash_reset();
				return;
			}

			int32_t repeat_cnt=0;
			if (json_pattern.containsKey("repeat_cnt")){
				repeat_cnt =json_pattern["repeat_cnt"].as<int>();
			}else {
				publishResponse("ERROR", "Missing repeat_cnt field");
				flash_reset();
				return;
			}

			pixel_t color_t;
			color_t.h=(int)h;
			color_t.s=(int)1000*s;
			color_t.l=(int)1000*l;
			color_t.w=(int)1000*w;
			SERIAL_PRINTF("Add flash segment: color=(%f,%f,%f), sPixel=%i, ePixel=%i, c_mode=%i, ton_ms=%i, toff_ms=%i, repeat_cnt=%i\n", h,s,l, sPixel, ePixel, c_mode, ton_ms, toff_ms, repeat_cnt);
			flash_addPattern(color_t, sPixel, ePixel, c_mode, ton_ms, toff_ms, repeat_cnt);
		}
	  }
	}

	publishResponse("OK", "Flash triggered");
}


void cLedStripe::mqttCallback_candle(const String &payload) {
	if (payload.length()>JSON_BUF_SIZE-2){
		publishResponse("ERROR", "Candle: Payload lenth to big: " + String(payload.length()));
		return;
	}

	auto error = deserializeJson(jsonBuffer, payload);
	if (error) {
		publishResponse("ERROR", "Candle: Payload missing or incorrect: " + String(error.c_str()));
		return;
	}

	if (c_eeprom.settings.n_pixels==0 || c_eeprom.settings.gpio==-1) {
		publishResponse("ERROR", "Strip not configured correctly: n_leds=" + String(c_eeprom.settings.n_pixels) + ", gpio= " + String(c_eeprom.settings.gpio));
		return;
	}

	  //Mandatory field state
	if (jsonBuffer.containsKey("state")) {
		if (jsonBuffer["state"].as<String>()=="OFF") {
			candle_reset();
			publishResponse("OK", "Stopped Candle");
			return;
		}
	}

	int32_t sPixel=0;
	if (jsonBuffer.containsKey("sPixel")) {
		sPixel=jsonBuffer["sPixel"].as<int>();
		//SERIAL_PRINTF("sPixel: %i\n", sPixel);
	} else {
		publishResponse("ERROR", "Candle: field sPixel missing");
		return;
	}

	int32_t ePixel=0;
	if (jsonBuffer.containsKey("ePixel")) {
		ePixel=jsonBuffer["ePixel"].as<int>();
		//SERIAL_PRINTF("ePixel: %i\n", ePixel);
	} else {
		publishResponse("ERROR", "Candle: field ePixel missing");
		return;
	}

	pixel_t pixel_1;
	String color_1;
	if (jsonBuffer.containsKey("color_1")) {
		color_1=jsonBuffer["color_1"].as<String>();
		SERIAL_PRINTLN("color_1: " + color_1);
	} else {
		publishResponse("ERROR", "Candle: field color_1 missing");
		return;
	}

	pixel_t pixel_2;
	String color_2;
	if (jsonBuffer.containsKey("color_2")) {
		color_2=jsonBuffer["color_2"].as<String>();
		SERIAL_PRINTLN("color_2: " + color_2);
	} else {
		publishResponse("ERROR", "Candle: field color_2 missing");
		return;
	}

	error = deserializeJson(jsonBuffer, color_1);
	if (error) {
		publishResponse("ERROR", "Candle: color_1 format incorrect: " + String(error.c_str()));
		return;
	} else {
		if (jsonBuffer.containsKey("h")) {
			pixel_1.h = 1000*jsonBuffer["h"].as<int>();
		}else {
			publishResponse("ERROR", "Mandatory field h missing from color_1");
			return;
		}

		if (jsonBuffer.containsKey("s")) {
			pixel_1.s = 1000*jsonBuffer["s"].as<float>();
		}else {
			publishResponse("ERROR", "Mandatory field s missing from color_1");
			return;
		}

		if (jsonBuffer.containsKey("l")) {
			pixel_1.l=1000*jsonBuffer["l"].as<float>();
		} else {
			publishResponse("ERROR", "Mandatory field l missing from color_1");
			return;
		}

		if (jsonBuffer.containsKey("w")) {
			pixel_1.w=1000*jsonBuffer["w"].as<float>();
		} else {
			pixel_1.w=0;
		}
	}

	error = deserializeJson(jsonBuffer, color_2);
	if (error) {
		publishResponse("ERROR", "Candle: color_2 format incorrect: " + String(error.c_str()));
		return;
	} else {
		if (jsonBuffer.containsKey("h")) {
			pixel_2.h = 1000*jsonBuffer["h"].as<int>();
		}else {
			publishResponse("ERROR", "Mandatory field h missing from color_2");
			return;
		}

		if (jsonBuffer.containsKey("s")) {
			pixel_2.s = 1000*jsonBuffer["s"].as<float>();
		}else {
			publishResponse("ERROR", "Mandatory field s missing from color_2");
			return;
		}

		if (jsonBuffer.containsKey("l")) {
			pixel_2.l=1000*jsonBuffer["l"].as<float>();
		} else {
			publishResponse("ERROR", "Mandatory field l missing from color_2");
			return;
		}

		if (jsonBuffer.containsKey("w")) {
			pixel_2.w=1000*jsonBuffer["w"].as<float>();
		} else {
			pixel_2.w=0;
		}
	}
	publishResponse("OK", String("Start Candle Effect for pixels [") + String(sPixel) + String(",") + String(ePixel) + String("[]"));
	candle_add(pixel_1, pixel_2, sPixel, ePixel);
}

/*
 {
  "state":"ON" or "OFF",
  "brightness": n,
  "transition": ms,
  "segments": "[
    '{ \"sPixel\": n,   \"ePixel\": n,  \"wrgb\":    \"0x\", \"brightness\":n}' -> multiple up to MAX_SEGMENTS
   ]"
}
 */
void cLedStripe::mqttCallback_set(const String &payload) {
	SERIAL_PRINTLN("mqttCallback_set: " + payload);

	if (strip==NULL){
		String emsg="Strip not configured, io=" + String(c_eeprom.settings.gpio) + ", n_leds=" + String(c_eeprom.settings.n_pixels);
		publishResponse("ERROR", emsg);
		return;
	}

	auto error = deserializeJson(jsonBuffer, payload);
	if (error) {
		publishResponse("ERROR", "SET: Payload missing or incorrect" + String(error.c_str()));
		return;
	}

	if (c_eeprom.settings.n_pixels==0 || c_eeprom.settings.gpio==-1) {
		publishResponse("ERROR", "Strip not configured correctly: n_leds=" + String(c_eeprom.settings.n_pixels) + ", gpio= " + String(c_eeprom.settings.gpio));
		return;
	}

	//Mandatory field state
	if (!jsonBuffer.containsKey("state")) {
		SERIAL_PRINTLN("ERROR: Mandatory field state missing");
		return;
	}

	run_reset();
	transitions_reset();
	flash_reset();
	c_eeprom.isOn=jsonBuffer["state"].as<String>()=="ON"?1:0;

	if (jsonBuffer.containsKey("transition")) {
		c_eeprom.transition_ms = jsonBuffer["transition"].as<int>();
		c_eeprom.transition_ms=c_eeprom.transition_ms>10000?10000:c_eeprom.transition_ms;
	}

	if (jsonBuffer.containsKey("segments")) {
		String str_seg=jsonBuffer["segments"];
		//SERIAL_PRINTLN("Segments: " + str_seg);
		auto error = deserializeJson(jsonBuffer, str_seg);
		if (error) {
			publishResponse("ERROR", "SET: Segments field incorrect" + String(error.c_str()));
			return;
		}

		// extract the values
		JsonArray array = jsonBuffer.as<JsonArray>();
		c_eeprom.n_segments=0;
		for(JsonVariant v : array) {
			StaticJsonDocument<200> json_segment;
			String segment=v.as<String>();
			//SERIAL_PRINTLN(segment);
			deserializeJson(json_segment, segment);

			if (json_segment.containsKey("h")) {
				c_eeprom.segments[c_eeprom.n_segments].h=1000*json_segment["h"].as<int>();
			}else {
				publishResponse("ERROR", "Mandatory field h missing");
				continue;
			}

			if (json_segment.containsKey("s")) {
				float s=1000*json_segment["s"].as<float>();
				c_eeprom.segments[c_eeprom.n_segments].s=(int)s;
			}else {
				publishResponse("ERROR", "Mandatory field s missing");
				continue;
			}

			if (json_segment.containsKey("l")) {
				float l=1000*json_segment["l"].as<float>();
				c_eeprom.segments[c_eeprom.n_segments].l=(int)l;
			} else {
				publishResponse("ERROR", "Mandatory field l missing");
				continue;
			}

			if (json_segment.containsKey("w")) {
				float w=1000*json_segment["w"].as<float>();
				c_eeprom.segments[c_eeprom.n_segments].w=(int)w;
			} else {
				c_eeprom.segments[c_eeprom.n_segments].w=0;
			}

			if (json_segment.containsKey("sPixel")) {
				c_eeprom.segments[c_eeprom.n_segments].sPixel=json_segment["sPixel"].as<int>();
			} else {
				publishResponse("ERROR", "Mandatory field sPixel missing");
				continue;
			}

			if (json_segment.containsKey("ePixel")) {
				c_eeprom.segments[c_eeprom.n_segments].ePixel=json_segment["ePixel"].as<int>();
			} else {
				publishResponse("ERROR", "Mandatory field ePixel missing");
				continue;
			}

			//SERIAL_PRINTF("Color: %i, %i, %i, %i\n",config.segments[config.n_segments].h,config.segments[config.n_segments].s,config.segments[config.n_segments].l, config.segments[config.n_segments].w);
			c_eeprom.n_segments++;
		}
	}

	stripUpdate();
	publishResponse("OK", "set");
}

void cLedStripe::listSegements() {
  for (int32_t i = 0; i < MAX_NUM_SEGMENTS; i++) {
      SERIAL_PRINT(i);
      SERIAL_PRINT(" : Start "); SERIAL_PRINT(c_eeprom.segments[i].sPixel);
      SERIAL_PRINT(", End: "); SERIAL_PRINT(c_eeprom.segments[i].ePixel);
      SERIAL_PRINTF(", h: %i, s: %i, l:%i\n",c_eeprom.segments[i].h,c_eeprom.segments[i].s,c_eeprom.segments[i].l);
  }
}

void cLedStripe::stripUpdate() {
	 //SERIAL_PRINTF("stripUpdate\n");
	 for (int i = 0; i < c_eeprom.n_segments; i++) {
		 pixel_t pix;
		 pix.l=c_eeprom.isOn?c_eeprom.segments[i].l:0;
		 pix.s=c_eeprom.segments[i].s;
		 pix.h=c_eeprom.segments[i].h;
		 pix.w=c_eeprom.isOn?c_eeprom.segments[i].w:0;
		 strip->updateLength(c_eeprom.settings.n_pixels);
		 SERIAL_PRINTF("stripUpdate - sPixel=%i, ePixel=%i, color=(%i,%i,%i,%i), n_leds=%i\n", c_eeprom.segments[i].sPixel, c_eeprom.segments[i].ePixel,pix.h,pix.s,pix.l,pix.w, strip->numPixels());
		 uint32_t heap=ESP.getFreeHeap();
		 if (heap>5000 || n_transitions<MAX_TRANS_EFFECTS){
			 SERIAL_PRINTF("------------------- Transition Start %i heap: %i-----------------\n",i,heap);
			 efects_transitions[i]=new cEffect_transition(
								 strip,
								 c_eeprom.segments[i].sPixel,
								 c_eeprom.segments[i].ePixel,
								 pixels,
								 pix,
								 c_eeprom.transition_ms);
			 n_transitions++;
		 } else {
			 SERIAL_PRINTF("Cannot start transition due to heap size: %i or amount of transitions: %i\n", heap, n_transitions);
		 }
	 }
}

void cLedStripe::loop_tick(int intervall_ms) {
	uint32_t done_t=0, done_f=0, done_w=0, done_c=0;

	time_elapsed_ms+=intervall_ms;

	if ((time_elapsed_ms)>RELAIS_PUBLISH_INT_S*1000){
		publishState();
		time_elapsed_ms=0;
	}

	//SERIAL_PRINTLN("tick");
	for (uint32_t i=0; i< n_runs; i++){
		if (run_patterns[i]!=NULL){
			if (!run_patterns[i]->loop()){
				delete run_patterns[i];
				run_patterns[i]=NULL;
				done_w++;
				SERIAL_PRINTF("RUN %i effect done, HEAP %i Bytes\n", i, ESP.getFreeHeap());
			}
		} else {
			done_w++;
		}
	}

	 for (uint32_t i=0; i< n_flashes; i++){
		if (efects_flash[i]!=NULL){
			if (!efects_flash[i]->loop()){
				delete efects_flash[i];
				efects_flash[i]=NULL;
				done_f++;
				SERIAL_PRINTF("Flash %i effect done, HEAP %i Bytes\n", i, ESP.getFreeHeap());

			}
		} else {
			done_f++;
		}
	}

	 for (uint32_t i=0; i< n_candles; i++){
		if (efects_candle[i]!=NULL){
			if (!efects_candle[i]->loop()){
				delete efects_candle[i];
				efects_candle[i]=NULL;
				done_c++;
				SERIAL_PRINTF("Candle %i effect done, HEAP %i Bytes\n", i, ESP.getFreeHeap());

			}
		} else {
			done_c++;
		}
	}

	for (uint32_t i=0; i< n_transitions;i++){
		if (efects_transitions[i]!=NULL){
			if (!efects_transitions[i]->loop()){
				delete efects_transitions[i];
				efects_transitions[i]=NULL;
				//done_t++;
				SERIAL_PRINTF("Transition %i effect done, HEAP %i Bytes\n", i, ESP.getFreeHeap());
				//MQTT_LOG("I", String("Transition done: ") + String(i));
				//strore
				c_eeprom.crc=calculate_crc((unsigned char*)&c_eeprom.layout_version, sizeof(c_eeprom)-sizeof(int));
				eepom_write_buf(e_address, (unsigned char*)&c_eeprom, sizeof(c_eeprom));
				//SERIAL_PRINTLN("OK: Stored Strip settings");
				//listSegements();
			}
		} else {
			done_t++;
		}
	}

	if (done_t < n_transitions || done_f<n_flashes || done_w<n_runs || done_c<n_candles) {
		//SERIAL_PRINTLN("update strip");
		strip->show();
	}
}


/******************************************************************************************************
 * RUN effect
 ******************************************************************************************************/
void cLedStripe::run_addPattern(pixel_t *color_buf, uint32_t buf_size, int32_t sPixel, int32_t ePixel, run_c_mode_t c_mode, run_t_mode_t t_mode, run_dir_mode_e dir, int32_t speed_ms){
	if (n_runs< MAX_RUN_EFFECTS){
		SERIAL_PRINTF("RUN_size: %i, heap size=%i\n", n_runs,ESP.getFreeHeap());
		//Verify ranges
		buf_size=(buf_size>MAX_PATTERN_LEDS)?MAX_PATTERN_LEDS:buf_size;
		if (sPixel<0) sPixel=0;
		if (sPixel>c_eeprom.settings.n_pixels) sPixel=c_eeprom.settings.n_pixels;
		if (ePixel<0) ePixel=0;
		if (ePixel>c_eeprom.settings.n_pixels) ePixel=c_eeprom.settings.n_pixels;

		run_patterns[n_runs] = new cEffect_run(strip, pixels, c_mode, t_mode, dir, sPixel, ePixel, color_buf, buf_size, speed_ms);
		n_runs++;
	} else {
		SERIAL_PRINTF("Maximum number of patterns reached: %i\n",n_runs);
	}
}

void cLedStripe::run_reset(){
	for (uint32_t i=0; i<n_runs;i++){
		if (run_patterns[i]!=NULL) delete run_patterns[i];
		run_patterns[i]=NULL;
	}
	n_runs=0;

	SERIAL_PRINTF("RUN Reset, heap: %i\n",ESP.getFreeHeap());

	//Reset pixels:
	for (int32_t i=0; i<c_eeprom.settings.n_pixels;i++){
		uint32_t pixel=hsl2rgbw((float)pixels[i].h, (float)pixels[i].s, (float)pixels[i].l, (float)pixels[i].w);
		strip->setPixelColor(i,WRGB2COLOR(pixel));
	}
	strip->show();
}

/*
 * Flash
 */
void cLedStripe::flash_addPattern(pixel_t color, int32_t sPixel, int32_t ePixel, run_c_mode_t c_mode, int32_t ton_ms, int32_t toff_ms, int32_t repeat_cnt){
	SERIAL_PRINTF("Add new pattern: %i\n",n_flashes);

	if (n_flashes<MAX_FLASH_EFFECTS){
		efects_flash[n_flashes]=new cEffect_flash(strip, pixels ,sPixel, ePixel, color,c_mode, ton_ms, toff_ms, repeat_cnt);
		n_flashes++;
	} else {
		SERIAL_PRINTF("Limit reached: %iX\n",MAX_FLASH_EFFECTS);
	}

/*	if (n_flashes==1) {
		os_timer_disarm(&ws2812_tickTimer);
		os_timer_arm(&ws2812_tickTimer,TRANS_TINT_MS , true);
	}*/
}

void cLedStripe::flash_reset(){
	for (uint32_t i=0; i< n_flashes;i++){
		if (efects_flash[i]!=NULL) delete efects_flash[i];
		efects_flash[i]=NULL;
	}
	n_flashes=0;

	//Reset pixels:
	for (int32_t i=0; i<=c_eeprom.settings.n_pixels;i++){
		uint32_t pixel=hsl2rgbw((float)pixels[i].h, (float)pixels[i].s, (float)pixels[i].l, (float)pixels[i].w);
		strip->setPixelColor(i,WRGB2COLOR(pixel));
	}
	strip->show();
}

void cLedStripe::candle_add(pixel_t color1, pixel_t color2, int32_t sPixel, int32_t ePixel){
	if (n_candles<MAX_CANDLE_EFFECTS){
		efects_candle[n_candles]=new cEffect_candle(strip, color1,color2, sPixel,ePixel);
		n_candles++;
	} else {
		SERIAL_PRINTF("Limit reached: %iX\n",MAX_CANDLE_EFFECTS);
	}
}

void cLedStripe::candle_reset(){
	for (uint32_t i=0; i< n_candles;i++){
		if (efects_candle[i]!=NULL) delete efects_candle[i];
		efects_candle[i]=NULL;
	}
	n_candles=0;

	//Reset pixels:
	for (int32_t i=0; i<=c_eeprom.settings.n_pixels;i++){
		uint32_t pixel=hsl2rgbw((float)pixels[i].h, (float)pixels[i].s, (float)pixels[i].l, (float)pixels[i].w);
		strip->setPixelColor(i,WRGB2COLOR(pixel));
	}
	strip->show();
}

void cLedStripe::transitions_reset(){
	for (uint32_t i=0; i< n_transitions;i++){
		if (efects_transitions[i]!=NULL) delete efects_transitions[i];
		efects_transitions[i]=NULL;
	}
	n_transitions=0;
}



