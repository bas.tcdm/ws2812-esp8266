#include <Arduino.h>
#include "persistent_storage.h"
#include "config.h"

#ifndef MQTT_H
#define MQTT_H

#define MQTT_MAX_PACKET_SIZE 1024

typedef enum {
	DEV_TYPE_BROKER,
	DEV_TYPE_LED,
	DEV_TYPE_EMETER,
	DEV_TYPE_OLED
}dev_type_e;

typedef enum {
  MQTT_NONE,
  MQTT_CLIENT,
  MQTT_BROKER
} mqtt_mode_e;

typedef struct {
	dev_type_e 	type;
	char		base_topic[MAX_TOPIC_LENTH];
}ext_device_t;

typedef struct{
  int				crc;
  char  			client_name[50];
  char  			wlan_ssid[50];
  char  			wlan_key[32];
  char  			wlan_gateway[50];
  char  			mqtt_broker_ip[50];
  char 				client_base_topic[MAX_TOPIC_LENTH];
  mqtt_mode_e 		mqtt_mode;
} client_config_t;

extern int mqtt_mode;

typedef void (*callback_Onconnection)(void);

extern void mqtt_broker_setup(void (*callback)(void), client_config_t *device_config);
extern void mqtt_broker_subscribe(const String &topic, void (*callback)(const String &topic, const String &payload));
extern void mqtt_broker_unsubscribe(const String &topic);
extern void mqtt_broker_publish(const String &topic, const String &payload);
extern void mqtt_broker_getRSSI(const char* ssid);
extern void mqtt_broker_loop(void);

extern void mqtt_client_setup(void (*callback)(void), client_config_t *device_config);
extern void mqtt_client_subscribe(const String &topic, void (*callback)(const String &topic, const String &payload));
extern void mqtt_client_unsubscribe(const String &topic);
extern void mqtt_client_publish(const String &topic, const String &payload);
extern void mqtt_client_loop(void);
extern void mqtt_client_getRSSI(const char* ssid);
extern bool mqtt_client_isConnected(void);

inline void mqtt_setup(void (*callback)(void), client_config_t *device_config){
	mqtt_mode=device_config->mqtt_mode;
	if (mqtt_mode==MQTT_CLIENT){
		mqtt_client_setup(callback, device_config);
	} else {
		mqtt_broker_setup(callback, device_config);
	}
}

inline void mqtt_subscribe(const String &topic, void (*callback)(const String &topic, const String &payload)){
	if (mqtt_mode==MQTT_CLIENT){
		mqtt_client_subscribe(topic, callback);
	} else {
		mqtt_broker_subscribe(topic, callback);
	}
}

inline void mqtt_unsubscribe(const String &topic){
	if (mqtt_mode==MQTT_CLIENT){
		mqtt_client_unsubscribe(topic);
	} else {
		mqtt_broker_unsubscribe(topic);
	}
}

inline void mqtt_publish(const String &topic, const String &payload){
	if (mqtt_mode==MQTT_CLIENT){
		mqtt_client_publish(topic, payload);
	} else {
		mqtt_broker_publish(topic, payload);
	}
}

inline int32_t wifi_getRSSI(void) {
	if (mqtt_mode==MQTT_CLIENT){
		return WiFi.RSSI();
	} else {return 0;}
}

inline String wifi_getIP(void) {
	return WiFi.localIP().toString();
}


inline void mqtt_loop(void) {
	if (mqtt_mode==MQTT_CLIENT){
		mqtt_client_loop();
	} else {
		mqtt_broker_loop();
	}
}

inline bool mqtt_isConnected(){
	if (mqtt_mode==MQTT_CLIENT){
		return mqtt_client_isConnected();
	} else {
		return true;
	}
}

#endif //MQTT_H
