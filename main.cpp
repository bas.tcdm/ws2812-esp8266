#include "cLedStripe.h"
#include "ota.h"
#include "mqtt.h"
#include "version.h"
#include <EEPROM.h>
#include <Ticker.h>
#include "cEmeter.h"
#include "cRelais.h"
#include "cTemperature.h"
#include "config.h"
#include "utils.h"
#include "persistent_storage.h"

#include <Wire.h>
#include <Adafruit_I2CDevice.h> 
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>
#include <Fonts/FreeSans9pt7b.h>
#include <Fonts/FreeSans12pt7b.h>


//External devices
static int	n_devices=0;
static cExtDevice *devices[TOTAL_EXT_DEVICES];

//Strips
static ledStrip_settings_t config_strips[N_STRIPS]=C_STRIPS;
static emeter_settings_t config_emeters[N_EMETERS]=C_EMETERS;
static relais_settings_t config_relais[N_RELAIS]=C_RELAIS;
static tsensor_settings_t config_tsensors[N_TSENSORS]=C_TSENSORS;

static client_config_t device_config;

#ifdef VARIANT_EMETER
#define OLED_RESET     -1 // Reset pin # (or -1 if sharing Arduino reset pin)
static Adafruit_SSD1306 display=Adafruit_SSD1306(OLED_WIDTH, OLED_HIGHT, &Wire, OLED_RESET);

// 'emeter', 128x64px
const unsigned char logo_emeter [] PROGMEM = {
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x07, 0xff, 0xf8, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x7f, 0xff, 0xff, 0x80, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x03, 0xfc, 0x00, 0x1f, 0xf0, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x0f, 0xc0, 0x00, 0x01, 0xfc, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x3e, 0x00, 0x00, 0x00, 0x3e, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x78, 0x00, 0x00, 0x00, 0x0f, 0x80, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x01, 0xf0, 0x00, 0x00, 0x00, 0x03, 0xc0, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x03, 0xc0, 0x00, 0x00, 0x00, 0x00, 0xf0, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x07, 0x80, 0x00, 0x00, 0x00, 0x00, 0x78, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x0f, 0x00, 0x00, 0x00, 0x00, 0xc7, 0x3c, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x1e, 0x00, 0x00, 0x00, 0x01, 0xef, 0x1c, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x1c, 0x00, 0x00, 0x00, 0x01, 0xef, 0x0e, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x38, 0x00, 0x00, 0x00, 0x01, 0xef, 0x0f, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x78, 0x00, 0x00, 0x60, 0x01, 0xef, 0x07, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x70, 0x00, 0x00, 0xf0, 0x01, 0xef, 0x03, 0x80, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x70, 0x00, 0x0c, 0xf3, 0x01, 0xef, 0x03, 0x80, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0xe0, 0x00, 0x1e, 0xf7, 0x81, 0xef, 0x03, 0x80, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0xe0, 0x03, 0xde, 0xf7, 0xbd, 0xef, 0x01, 0xc0, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0xe0, 0x03, 0xde, 0xf7, 0xbd, 0xef, 0x01, 0xc0, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0xe0, 0x7b, 0xde, 0xf7, 0xbd, 0xef, 0x01, 0xc0, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0xe0, 0x7b, 0xde, 0xf7, 0xbd, 0xef, 0x01, 0xc0, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0xe0, 0x7b, 0xde, 0xf7, 0xbd, 0xef, 0x01, 0xc0, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0xe0, 0x7b, 0xde, 0xf7, 0xbd, 0xef, 0x01, 0xc0, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0xe0, 0x7b, 0xde, 0xf7, 0xbd, 0xef, 0x03, 0xc0, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0xe0, 0x7b, 0xde, 0xf7, 0xbd, 0xef, 0x03, 0x80, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x70, 0x7b, 0xde, 0xf7, 0xbd, 0xef, 0x03, 0x80, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x70, 0x7b, 0xde, 0xf7, 0xbd, 0xef, 0x07, 0x80, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x38, 0x7b, 0xde, 0xf7, 0xbd, 0xef, 0x07, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x38, 0x7b, 0xde, 0xf7, 0xbd, 0xef, 0x0e, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x1c, 0x7b, 0xde, 0xf7, 0xbd, 0xef, 0x1e, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x0e, 0x31, 0x8c, 0x63, 0x18, 0xc6, 0x1c, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x0f, 0x00, 0x00, 0x00, 0x00, 0x00, 0x38, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x07, 0x80, 0x00, 0x00, 0x00, 0x00, 0xf0, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x03, 0xe0, 0x00, 0x00, 0x00, 0x01, 0xe0, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0xf0, 0x00, 0x00, 0x00, 0x07, 0xc0, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x7c, 0x00, 0x00, 0x00, 0x0f, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x1f, 0x00, 0x00, 0x00, 0x7e, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x07, 0xf0, 0x00, 0x03, 0xf8, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0xff, 0x80, 0x7f, 0xe0, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x3f, 0xff, 0xff, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0xff, 0xe0, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x03, 0x80, 0x0c, 0x00, 0x00, 0x40, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x07, 0xc0, 0x1e, 0x00, 0x00, 0xe0, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x07, 0xe0, 0x3e, 0x00, 0x00, 0xe0, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x1f, 0x07, 0xe0, 0x7e, 0x07, 0xc3, 0xf8, 0x3e, 0x0c, 0xc0, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x3f, 0xc7, 0x70, 0x7e, 0x1f, 0xf3, 0xf8, 0xff, 0x0f, 0xc0, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x70, 0xe7, 0x38, 0xee, 0x38, 0x30, 0xe1, 0xc1, 0x8e, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0xe0, 0xe7, 0x39, 0xce, 0x38, 0x38, 0xe1, 0xc1, 0xce, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0xff, 0xe7, 0x1d, 0xce, 0x3f, 0xf8, 0xe1, 0xff, 0xce, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0xe0, 0x07, 0x0f, 0x8e, 0x38, 0x00, 0xe1, 0xc0, 0x0e, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x70, 0x67, 0x0f, 0x8e, 0x1c, 0x30, 0xe1, 0xc1, 0x8e, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x3f, 0xc7, 0x07, 0x0e, 0x0f, 0xf0, 0xfc, 0xff, 0x8e, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x0f, 0x00, 0x00, 0x00, 0x03, 0xc0, 0x38, 0x1e, 0x04, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
};

//Signal Strength
#define RSSI_HEIGHT   12
#define RSSI_WIDTH    19
// 'signal_00', 19x12px
const unsigned char rssi_00 [] PROGMEM = {
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00
};
// 'signal_20', 19x12px
const unsigned char rssi_20 [] PROGMEM = {
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xe0, 0x00,
	0x00, 0xe0, 0x00, 0x00
};
// 'signal_40', 19x12px
const unsigned char rssi_40 [] PROGMEM = {
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x0e, 0x00, 0x00, 0x0e, 0x00, 0x00, 0x0e, 0x00, 0x00, 0xee, 0x00,
	0x00, 0xee, 0x00, 0x00
};
// 'signal_60', 19x12px
const unsigned char rssi_60 [] PROGMEM = {
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xe0, 0x00, 0x00,
	0xe0, 0x00, 0x00, 0xe0, 0x00, 0x0e, 0xe0, 0x00, 0x0e, 0xe0, 0x00, 0x0e, 0xe0, 0x00, 0xee, 0xe0,
	0x00, 0xee, 0xe0, 0x00
};
// 'signal_80', 19x12px
const unsigned char rssi_80 [] PROGMEM = {
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x0e, 0x00, 0x00, 0x0e, 0x00, 0x00, 0xee, 0x00, 0x00,
	0xee, 0x00, 0x00, 0xee, 0x00, 0x0e, 0xee, 0x00, 0x0e, 0xee, 0x00, 0x0e, 0xee, 0x00, 0xee, 0xee,
	0x00, 0xee, 0xee, 0x00
};
// 'signal_100', 19x12px
const unsigned char rssi_100 [] PROGMEM = {
	0x00, 0x00, 0xe0, 0x00, 0x00, 0xe0, 0x00, 0x0e, 0xe0, 0x00, 0x0e, 0xe0, 0x00, 0xee, 0xe0, 0x00,
	0xee, 0xe0, 0x00, 0xee, 0xe0, 0x0e, 0xee, 0xe0, 0x0e, 0xee, 0xe0, 0x0e, 0xee, 0xe0, 0xee, 0xee,
	0xe0, 0xee, 0xee, 0xe0
};

#endif

//Ticker
static Ticker loop_tick;

static StaticJsonDocument<JSON_BUF_SIZE> jsonBuffer;

static os_timer_t tickTimer;
static int tickCounter = 0;
static bool tickOccured = false;
static uint32  rstReason;

void tickTimerCallback(void *pArg){ 
  tickOccured = true;
  *((int *) pArg) += 1;
}

void publishState() {
	if (!mqtt_isConnected()){
		SERIAL_PRINTLN("Lost Connection to Network");
		return;
	}
  String msg = "{";
  msg.concat("  \"client_name\": \""   + String(device_config.client_name) + "\"");
  msg.concat(", \"wlan_ssid\": \""     + String(device_config.wlan_ssid) + "\"");
  msg.concat(", \"wlan_rssi\": \""     + String(wifi_getRSSI()) + "\"");
  msg.concat(", \"wlan_ip\": \""       + wifi_getIP() + "\"");
  msg.concat(", \"wlan_gateway\": \""  + String(device_config.wlan_gateway) + "\"");
  msg.concat(", \"mqtt_broker_ip\": \""+ String(device_config.mqtt_broker_ip) + "\"");
  msg.concat(", \"base_topic\": \""    + String(device_config.client_base_topic) + "\"");
  msg.concat(", \"n_devices\":"    	   + String(TOTAL_EXT_DEVICES));
  msg.concat(", \"version\": \""    	   + String(VERSION)+ "\"");
  msg.concat("}");
  mqtt_publish(device_config.client_base_topic, msg);
}

void publishResponse(String code, String msg) {
	if (!mqtt_isConnected()){
		SERIAL_PRINTLN("Lost Connection to Network");
		return;
	}
  //String topic=String(device_config.base_topic);
	mqtt_publish(String(device_config.client_base_topic) + "/response", "{ \"code\": \"" + code + "\", \"msg\":\" " + msg + "\" }" );
}

void mqtt_log(String level, String msg){
	if (!mqtt_isConnected()){
			SERIAL_PRINTLN("Lost Connection to Network");
			return;
	}
	//String topic=String(device_config.base_topic);
	mqtt_publish(String(device_config.client_base_topic) + "/log", "{ \"level\": \"" + level + "\", \"msg\":\" " + msg + "\" }" );
}

/*
 * Reset network and strip settings
 {}
 */
void mqttCallback_rst_config(const String &topic, const String &payload) {
	(void)topic;
	SERIAL_PRINTLN("mqttCallback_reset: " + payload);
	auto error = deserializeJson(jsonBuffer, payload);
	if (error) {
		SERIAL_PRINT(F("deserializeJson() failed with code "));
		SERIAL_PRINTLN(error.c_str());
		publishResponse("ERROR", "RESET: Payload missing or incorrect");
		return;
	}
	memset((char *)&device_config, 0, sizeof(device_config));
	eepom_write_buf(EEPROM_BASE_ADDRESS, (unsigned char*)&device_config, sizeof(device_config));

	for (int i=0;i<TOTAL_EXT_DEVICES;i++){
		if (devices[i]!=NULL){
			devices[i]->reset_config();
		}
	}

	ESP.reset();
}

/*
 * Reset device
 {}
 */
void mqttCallback_rst_device(const String &topic, const String &payload) {
	(void)topic;
	SERIAL_PRINTLN("mqttCallback_reset: " + payload);
	auto error = deserializeJson(jsonBuffer, payload);
	if (error) {
		SERIAL_PRINT(F("deserializeJson() failed with code "));
		SERIAL_PRINTLN(error.c_str());
		publishResponse("ERROR", "RESET: Payload missing or incorrect");
		return;
	}

	if (!jsonBuffer.containsKey("base_topic")) {
		publishResponse("ERROR", "RESET DEV: Missing field base_topic");
		SERIAL_PRINTF("Configure base_topic: %s\n", device_config.client_base_topic);
	}

	String base_topic=jsonBuffer["base_topic"].as<String>();
    if (base_topic==String(device_config.client_base_topic)){
    	ESP.reset();
    } else {
    	publishResponse("ERROR", "RESET DEV: Wrong base topic: " + base_topic);
    }
}


/***************************************************
{
  "nLeds"         : n,
  "client_name"   : "",
  "wlan_ssid"     : "",
  "wlan_key"      : "",
  "wlan_gateway"  : "",
  "mqtt_broker_ip": "",
  "base_topic"    : "",
  "mqtt_mode"     : "MQTT_CLIENT" or "MQTT_BROKER"
}
 ****************************************************/
void mqttCallback_mqttconfig(const String &topic, const String &payload) {
	(void)topic;
  SERIAL_PRINTLN("mqttCallback_config: " + payload);
  auto error = deserializeJson(jsonBuffer, payload);
  if (error) {
    SERIAL_PRINT(F("deserializeJson() failed with code "));
    SERIAL_PRINTLN(error.c_str());
    publishResponse("ERROR", "SET: Payload missing or incorrect");
    return;
  }

  if (jsonBuffer.containsKey("client_name")) {
    String client_name=jsonBuffer["client_name"].as<String>();
    client_name.toCharArray(device_config.client_name,50);
    SERIAL_PRINTF("Configure client_name: %s\n", device_config.client_name);
  }

  if (jsonBuffer.containsKey("wlan_ssid")) {
    String wlan_ssid=jsonBuffer["wlan_ssid"].as<String>();
    wlan_ssid.toCharArray(device_config.wlan_ssid,50);
    SERIAL_PRINTF("Configure wlan_ssid: %s\n", device_config.wlan_ssid);
  }

  if (jsonBuffer.containsKey("wlan_key")) {
    String wlan_key=jsonBuffer["wlan_key"].as<String>();
    wlan_key.toCharArray(device_config.wlan_key,50);
    SERIAL_PRINTF("Configure wlan_key: %s\n", device_config.wlan_key);
  }

  if (jsonBuffer.containsKey("wlan_gateway")) {
    String wlan_gateway=jsonBuffer["wlan_gateway"].as<String>();
    wlan_gateway.toCharArray(device_config.wlan_gateway,50);
    SERIAL_PRINTF("Configure wlan_gateway: %s\n", device_config.wlan_gateway);
  }

  if (jsonBuffer.containsKey("mqtt_broker_ip")) {
    String mqtt_broker_ip=jsonBuffer["mqtt_broker_ip"].as<String>();
    mqtt_broker_ip.toCharArray(device_config.mqtt_broker_ip,50);
    SERIAL_PRINTF("Configure mqtt_broker_ip: %s\n", device_config.mqtt_broker_ip);
  }

  if (jsonBuffer.containsKey("base_topic")) {
    String base_topic=jsonBuffer["base_topic"].as<String>();
    base_topic.toCharArray(device_config.client_base_topic,50);
    SERIAL_PRINTF("Configure base_topic: %s\n", device_config.client_base_topic);
  }

  if (jsonBuffer.containsKey("mqtt_mode")) {
    String mode=jsonBuffer["mqtt_mode"].as<String>();
    if (mode==String("MQTT_CLIENT")){
      device_config.mqtt_mode = MQTT_CLIENT;
      SERIAL_PRINTF("Configure mqtt_mode: MQTT_CLIENT\n");
    } else {
      device_config.mqtt_mode = MQTT_BROKER;
      SERIAL_PRINTF("Configure mqtt_mode: MQTT_BROKER\n");
    }
  }

  device_config.crc=calculate_crc((unsigned char *)device_config.client_name, (unsigned int)(sizeof(device_config) - sizeof(device_config.crc)));
  eepom_write_buf(EEPROM_BASE_ADDRESS, (unsigned char*)&device_config, sizeof(client_config_t));
  publishResponse("OK", "config");
  ESP.restart();
}

void reset_device_settings() {
	SERIAL_PRINTLN("Write Defaults to EEPROM: ");
	String client_name=String(MQTT_CLIENT_NAME);
	client_name.toCharArray((char*)&device_config.client_name,sizeof(device_config.client_name));
	String wlan_ssid=String(MQTT_CLIENT_SSID);
	wlan_ssid.toCharArray((char*)&device_config.wlan_ssid,sizeof(device_config.wlan_ssid));
	(String(MQTT_CLIENT_KEY)).toCharArray((char*)&device_config.wlan_key,sizeof(device_config.wlan_key));
	String(MQTT_CLIENT_GW).toCharArray((char*)&device_config.wlan_gateway, sizeof(device_config.wlan_gateway));
	String(MQTT_CLIENT_IP).toCharArray((char*)&device_config.mqtt_broker_ip,sizeof(device_config.mqtt_broker_ip));
	String("wifi2mqtt/" + client_name).toCharArray((char*)&device_config.client_base_topic,sizeof(device_config.client_base_topic));
	device_config.mqtt_mode=MQTT_CLIENT_MODE;

	device_config.crc=calculate_crc((unsigned char *)device_config.client_name, (unsigned int)(sizeof(device_config) - sizeof(device_config.crc)));
	eepom_write_buf(EEPROM_BASE_ADDRESS, (unsigned char*)&device_config, sizeof(client_config_t));

	SERIAL_PRINTF("CRC: %i\n", device_config.crc);
	SERIAL_PRINTF("client_name: %s\n", device_config.client_name);
	SERIAL_PRINTF("wlan_ssid: %s\n", device_config.wlan_ssid);
	SERIAL_PRINTF("wlan_key: %s\n", device_config.wlan_key);
	SERIAL_PRINTF("wlan_gateway: %s\n", device_config.wlan_gateway);
	SERIAL_PRINTF("mqtt_broker_ip: %s\n", device_config.mqtt_broker_ip);
	SERIAL_PRINTF("base_topic: %s\n", device_config.client_base_topic);
	SERIAL_PRINTF("mqtt_mode: %i\n", device_config.mqtt_mode);
}

#ifdef VARIANT_EMETER
#define RSSI_AVG_LENTH 	100
int rssi_avg_buf[RSSI_AVG_LENTH];
int n_rssi_avg=0;
void update_rssi(int rssi){

	rssi_avg_buf[n_rssi_avg % RSSI_AVG_LENTH]=rssi;
	n_rssi_avg++;

	int rssi_avg=0;
	for (int i=0;i<RSSI_AVG_LENTH;i++){
		rssi_avg+=rssi_avg_buf[i];
	}
	rssi_avg=rssi_avg/RSSI_AVG_LENTH;

	display.fillRect((display.width()  - RSSI_WIDTH ), 0 , RSSI_WIDTH, RSSI_HEIGHT, 0);

	if (rssi_avg<=-90){
		display.drawBitmap(	(display.width()  - RSSI_WIDTH ),0, rssi_00, RSSI_WIDTH, RSSI_HEIGHT, 1);
	}
	else if (rssi_avg > -90 && rssi_avg<=-80){
		display.drawBitmap(	(display.width()  - RSSI_WIDTH ),0, rssi_20, RSSI_WIDTH, RSSI_HEIGHT, 1);
	}
	else if (rssi_avg > -80 && rssi_avg<=-70){
		display.drawBitmap(	(display.width()  - RSSI_WIDTH ),0, rssi_40, RSSI_WIDTH, RSSI_HEIGHT, 1);
	}
	else if (rssi_avg > -70 && rssi_avg<=-60) {
		display.drawBitmap(	(display.width()  - RSSI_WIDTH ),0, rssi_60, RSSI_WIDTH, RSSI_HEIGHT, 1);
	}
	else if (rssi_avg > -60 && rssi_avg<=-50) {
		display.drawBitmap(	(display.width()  - RSSI_WIDTH ),0, rssi_80, RSSI_WIDTH, RSSI_HEIGHT, 1);
	}
	else if (rssi_avg > -50){
		display.drawBitmap(	(display.width()  - RSSI_WIDTH ),0, rssi_100, RSSI_WIDTH, RSSI_HEIGHT, 1);
	}
}

void update_energy(){
	float power=0.0, energy=0.0;
	for (int i=0; i<TOTAL_EXT_DEVICES;i++){
		if (devices[i]!=NULL){
			power+=devices[i]->get_power();
			energy+=devices[i]->get_energy();
		} else {
			SERIAL_PRINTF("Device==NULL\n");
		}
	}


	display.fillRect(0, 28 , display.width(), display.height() - 28, 0);
	String kWh = String(energy, 2) + String(" kWh");
	display.setTextColor(WHITE);
	display.setCursor(0, 40);
	display.println(kWh);
	display.setCursor(0, 63);
	String kW = String(power,2) + String(" kW");
	display.println(kW);
	//display.display();
}
#endif

void init_devices(){
	String base_topic=String(device_config.client_base_topic);

	int eeprom_add=EEPROM_BASE_ADDRESS + sizeof(client_config_t);

	n_devices=0;
	for (int i=0; i<N_RELAIS;i++){
		SERIAL_PRINTF("------------------- RELAIS %i -----------------------------------\n",i);
		if (devices[n_devices]!=NULL) {
			delete devices[n_devices];
		}
		devices[n_devices]=new cRelais(eeprom_add, base_topic, config_relais[i]);
		n_devices++;
		eeprom_add+=sizeof(relais_eeprom_t);
	}

	for (int i=0; i<N_STRIPS;i++){
		SERIAL_PRINTF("-------------------- STRIPS %i ----------------------------------\n", i);
		if (devices[n_devices]!=NULL) delete devices[n_devices];
		devices[n_devices] = new cLedStripe(eeprom_add, base_topic, config_strips[i]);
		n_devices++;
		eeprom_add+=sizeof(ledStripe_eeprom_t);
	}

	for (int i=0; i<N_EMETERS;i++){
		SERIAL_PRINTF("-------------------- EMETERS %i -----------------------------------\n",i);
		if (devices[n_devices]!=NULL) delete devices[n_devices];
		devices[n_devices] = new cEmeter(eeprom_add, base_topic, config_emeters[i]);
		n_devices++;
		eeprom_add+=sizeof(eMeter_eeprom_t);
	}

	for (int i=0; i<N_TSENSORS;i++){
		SERIAL_PRINTF("-------------------- TSENSORS %i -----------------------------------\n",i);
		if (devices[n_devices]!=NULL) delete devices[n_devices];
		devices[n_devices] = new cTemperature(eeprom_add, base_topic, config_tsensors[i]);
		n_devices++;
		eeprom_add+=sizeof(tsensor_eeprom_t);
	}

	  os_timer_setfn(&tickTimer, tickTimerCallback, &tickCounter);
	  os_timer_disarm(&tickTimer);
	  os_timer_arm(&tickTimer, TICK_INT_MS, true); //60 sec
}

bool connected=false;
void mqtt_onConnectionEstablished() {
	SERIAL_PRINTF("MQTT New connection established\n");
	if (connected) {
		//Lost connection and reconnected again, unsubscribe
		SERIAL_PRINTLN("Reconnected MQTT connection");
		MQTT_LOG("I", "Reconnected to Broker");
		for (int i=0;i<TOTAL_EXT_DEVICES;i++){
			if (devices[i]!=NULL){
				devices[i]->mqtt_disconnected();
			} else {
				SERIAL_PRINTF("Device %i == NULL\n", i);
			}
		}
		//MQTT_LOG("D", "Disconnected all devices");
	} else {
		  String msg=String("Device Booted");
		  switch (rstReason) {
			  case REASON_DEFAULT_RST: /* normal startup by power on */
				  msg+= String(", Reset Reason: REASON_DEFAULT_RST");
				  break;
			  case REASON_WDT_RST: /* hardware watch dog reset */
				  msg+= String(", Reset Reason: REASON_WDT_RST");
				  break;
			  case REASON_EXCEPTION_RST: /* exception reset, GPIO status won't change */
				  msg+= String(", Reset Reason: REASON_EXCEPTION_RST");
				  break;
			  case REASON_SOFT_WDT_RST: /* software watch dog reset, GPIO status won't change */
				  msg+= String(", Reset Reason: REASON_SOFT_WDT_RST");
				  break;
			  case REASON_SOFT_RESTART: /* software restart ,system_restart , GPIO status won't change */
				  msg+= String(", Reset Reason: REASON_SOFT_RESTART");
				  break;
			  case REASON_DEEP_SLEEP_AWAKE: /* wake up from deep-sleep */
				  msg+= String(", Reset Reason: REASON_DEEP_SLEEP_AWAKE");
				  break;
			  case REASON_EXT_SYS_RST: /* external system reset */
				  msg+= String(", Reset Reason: REASON_EXT_SYS_RST");
				  break;
			  default:
				  msg+= String(", Reset Reason: UNKNOWN");
				  break;
		  };
		 MQTT_LOG("I", msg);
		 publishState();
		 SERIAL_PRINTF("Up and running\n");
	}

#ifdef VARIANT_EMETER
	  display.clearDisplay();

	  //Display IP
	  display.setFont(&FreeSans9pt7b);
	  display.setTextSize(1); // Draw 2X-scale text
	  display.setTextColor(WHITE);
	  display.setCursor(0, 12);
	  display.println(wifi_getIP());
	  display.drawLine(0, 18, display.width(), 18, WHITE);
	  display.drawLine(0, 19, display.width(), 19, WHITE);
#endif

	connected=true;
	String base_topic=String(device_config.client_base_topic);
	mqtt_subscribe(base_topic + "/mqttconfig", mqttCallback_mqttconfig);
	mqtt_subscribe(base_topic + "/reset_config", mqttCallback_rst_config);
	mqtt_subscribe(base_topic + "/reset_device", mqttCallback_rst_device);

	for (int i=0;i<TOTAL_EXT_DEVICES;i++){
		if (devices[i]!=NULL){
			SERIAL_PRINTF("Connect device %i\n", i);
			devices[i]->mqtt_connected();
		} else {
			SERIAL_PRINTF("Device %i == NULL\n", i);
		}
	}
	//MQTT_LOG("D", "All devices connected to broker");

	SERIAL_PRINTLN("--------------------------------------------");
}


void setup() {
	SERIAL_BEGIN
	rst_info *rstInfo=ESP.getResetInfoPtr();
	rstReason = rstInfo->reason;
	ESP.wdtEnable(10000);
#ifdef VARIANT_EMETER
	if(!display.begin(SSD1306_SWITCHCAPVCC, OLED_I2C_ADDR)) {
		SERIAL_PRINTF("Could not activate OLED\n");
	} else {
		  display.clearDisplay();
		  display.drawBitmap(
			(display.width()  - OLED_WIDTH ) / 2,
			(display.height() - OLED_HIGHT) / 2,
			logo_emeter, OLED_WIDTH, OLED_HIGHT, 1);
		  display.display();
	}
#endif
	eeprom_init();
	eeprom_read_buf(EEPROM_BASE_ADDRESS, (unsigned char*)&device_config, sizeof(client_config_t));
	int crc=calculate_crc((unsigned char*)device_config.client_name, sizeof(client_config_t)-sizeof(device_config.crc));
	if (crc!=device_config.crc || device_config.crc==0){
		SERIAL_PRINTF("WRONG CRC: RESET SETTINGS, CRC=%i\n", crc);
		reset_device_settings();
	}
	//reset_device_settings();
	SERIAL_PRINTF("\nInitialise MQTT, mode=%i\n", device_config.mqtt_mode);
	mqtt_setup(mqtt_onConnectionEstablished, &device_config);

	ota_setup();
	init_devices();
	SERIAL_PRINTLN("Done intital setup");

}

uint publish_cnt=0;
void loop() {
	//mqtt_loop();
	ota_loop();
	if (!tickOccured) return;
	tickOccured=false;
	mqtt_loop();

#ifdef VARIANT_EMETER
	if (!connected && device_config.mqtt_mode==MQTT_CLIENT){
		display.clearDisplay();
		display.setCursor(0, 40);
		String msg=String("Waiting for connection...");
		display.println(msg);
	} else {
		update_rssi(wifi_getRSSI());
		update_energy();
		display.display();
	}
#endif

	//SERIAL_PRINTLN("tick devices");
	//SERIAL_PRINTF("Tick, n_devices=%i\n", n_devices);

	for (int i=0;i<TOTAL_EXT_DEVICES;i++){
		if (devices[i]!=NULL){
			devices[i]->loop_tick(TICK_INT_MS);
		} else {
			SERIAL_PRINTF("Device %i == NULL\n", i);
		}
		//mqtt_loop();
	}


	if (mqtt_isConnected() && ((publish_cnt*TICK_INT_MS) > 60000)){ //Every 5 minutes
		//SERIAL_PRINTF("publishState, publish_cnt=%i\n",publish_cnt);
		publishState();
		publish_cnt=0;
		//mqtt_loop();
	} else if (mqtt_isConnected()) {
		publish_cnt++;
	}

	if (tickOccured) SERIAL_PRINTF("Timer to fast\n");
}
