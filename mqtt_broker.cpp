#include "mqtt.h"
#include "eeprom.h"
#include "config.h"
#include <Arduino.h>
#include <uMQTTBroker.h>
#include <MQTT.h>
#include <map>

callback_Onconnection mqtt_broker_Onconnection;

typedef void (*callback_onTopic)(const String &topic, const String &data);
typedef std::map<String, callback_onTopic> t_topics;
t_topics registered_topics;

int mqtt_mode;

class myMQTTBroker: public uMQTTBroker
{

public:
	myMQTTBroker(){};
	virtual ~myMQTTBroker(){};
    virtual bool onConnect(IPAddress addr, uint16_t client_count) {
      (void)client_count;
      SERIAL_PRINTLN(addr.toString()+" connected");
      (void)addr;
      if (mqtt_broker_Onconnection!=NULL) {
    	  mqtt_broker_Onconnection();
      } else {
    	  SERIAL_PRINTLN("mqtt_broker_Onconnection == NULL");
    	  return false;
      }
      return true;
    }
    
    virtual bool onAuth(String username, String password) {
    	(void)(username);(void)password;
      SERIAL_PRINTLN("Username/Password: "+username+"/"+password);
      return true;
    }
    
    virtual void onData(String topic, const char *data, uint32_t length) {
      char data_str[length+1];
      os_memcpy(data_str, data, length);
      data_str[length] = '\0';
      String payload(data_str);
      //SERIAL_PRINTLN("received topic '"+topic+"' with data '"+payload+"'");
      callback_onTopic callback=registered_topics[topic];
      if (callback!=NULL) {
        callback(topic, payload);
      } else {
        SERIAL_PRINTLN("callback for topic "+ topic + " equal to NULL");
      }
    }
};

myMQTTBroker myBroker;

void startWiFiAP(client_config_t *broker_config)
{
  IPAddress ip,gw;
  ip.fromString(broker_config->mqtt_broker_ip);
  gw.fromString(broker_config->wlan_gateway);
  
  WiFi.softAPConfig(ip,gw,IPAddress(255,255,255,0));
  WiFi.softAP(broker_config->wlan_ssid, broker_config->wlan_key);
  SERIAL_PRINTF("AP started, ssid: %s\n", broker_config->wlan_ssid);
  SERIAL_PRINTLN("IP address: " + WiFi.softAPIP().toString());
  String wlan_key= String(broker_config->wlan_key);
  SERIAL_PRINTLN("NW Key: " + wlan_key);
}

void mqtt_broker_setup(void (*callback)(void), client_config_t *broker_config){
  mqtt_broker_Onconnection=callback;
  startWiFiAP(broker_config);
  SERIAL_PRINTLN("Started MQTT broker");
  myBroker.init();
}

void mqtt_broker_subscribe(const String &topic, void (*callback)(const String &topic, const String &payload)){
  myBroker.subscribe(topic);
  SERIAL_PRINTLN("Subscribe to " + topic);
  registered_topics[topic]=callback;
}

void mqtt_broker_unsubscribe(const String &topic){
  myBroker.unsubscribe(topic);
  SERIAL_PRINTLN("Unsubscribe to " + topic);
  registered_topics[topic]=NULL;
}

void mqtt_broker_publish(const String &topic, const String &payload){
  myBroker.publish(topic, payload);
}

void mqtt_broker_loop(void){
  
}
