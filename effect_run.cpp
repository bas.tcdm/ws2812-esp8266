/*
 * effect_run.cpp
 *
 *  Created on: 19.11.2020
 *      Author: swielens
 */


#include "cLedStripe.h"
#include "effect.h"
#include "utils.h"

cEffect_run::cEffect_run(Adafruit_NeoPixel *strip, pixel_t *pix,
	run_c_mode_t c_m,	//Color Mode
	run_t_mode_t t_m,  //Tail mode
	run_dir_mode_e  d,	//Direction
	int32_t  sPix,		//Start pixel
	int32_t  ePix,		//End pixel
	pixel_t  *pBuf,		//Pattern buffer
	int32_t  psize,		//Pattern size
	int32_t  stp_ms)	//Step size in ms
{
	neo_strip=strip;
	c_mode=c_m;
	t_mode=t_m;
	dir_mode=d;
	dir_mode_int=d;
	sPixel=sPix;
	ePixel=ePix;
	if (psize>MAX_PATTERN_LEDS) psize=MAX_PATTERN_LEDS;

	memcpy(pattern_buf,pBuf,psize*sizeof(pixel_t));
	p_size=psize;
	pixels=pix;
	step_ms=stp_ms;

	uint32_t tmp=ePixel;
	SERIAL_PRINTF("tmp: %i\n", tmp);
	switch (dir_mode) {
		case RUN_DIR_PINGPONG:
			dir_mode_int=RUN_DIR_RIGHT;
			ePixel=(ePixel>sPixel)?tmp:sPixel;
			sPixel=(ePixel>sPixel)?sPixel:tmp;
			_position=sPixel;
			break;
		case RUN_DIR_RIGHT:
			ePixel=(ePixel>sPixel)?tmp:sPixel;
			sPixel=(ePixel>sPixel)?sPixel:tmp;
			_position=sPixel;
			break;
		case RUN_DIR_LEFT:
			ePixel=(ePixel>sPixel)?sPixel:tmp;
			sPixel=(ePixel>sPixel)?tmp:sPixel;
			_position=sPixel;
			break;
		default:
			break;
	}
	_t_elapsed=0;
	SERIAL_PRINTF("New run Object: sPixel=%i, ePixel=%i, size=%i, stp_ms=%i, pattern_buf[0]=(%f,%f,%f,%f)\n", sPixel, ePixel, p_size, stp_ms, ((float)pattern_buf[0].h)/1000, ((float)pattern_buf[0].s)/1000, ((float)pattern_buf[0].l)/1000, ((float)pattern_buf[0].w)/1000);
	//SERIAL_PRINTF("New run Object: sPixel=%i, ePixel=%i, size=%i, stp_ms=%i, pattern_buf[1]=(%f,%f,%f,%f)\n", sPixel, ePixel, p_size, stp_ms, pattern_buf[1].h, pattern_buf[1].s, pattern_buf[1].l, pattern_buf[0].w);
}

cEffect_run::~cEffect_run() {}

bool cEffect_run::shiftRight(){
	for (int32_t i=_position-p_size;i<_position;i++){
		int32_t	p=i-_position+p_size;
		if (i>=0 && i < neo_strip->numPixels() && i>=sPixel && i<ePixel){
			uint32_t pixel;
			if (c_mode==RUN_MODE_COLOR) {
				pixel = hsl2rgbw(pattern_buf[p].h, pattern_buf[p].s, pattern_buf[p].l, pattern_buf[p].w);
			}
			else {
				pixel = hsl2rgbw(pixels[i].h, pixels[i].s, pixels[i].l+ pattern_buf[p].l,pattern_buf[p].l);
			}
			neo_strip->setPixelColor(i,WRGB2COLOR(pixel));
		}
	}
	if (_position>1 && t_mode==RUN_TAIL_OLD){
		//Reset pixel to original color
		uint32_t pixel = hsl2rgbw(pixels[_position-p_size - 1].h, pixels[_position- p_size-1].s, pixels[_position-p_size-1].l, pixels[_position-p_size-1].w);
		neo_strip->setPixelColor(_position-p_size-1,WRGB2COLOR(pixel));
	}
	//neo_strip->show();
	_position++;
	if (_position>ePixel+p_size) return true; //Reached end pixel
	else return false;
}

bool cEffect_run::shiftLeft(){
	for (int32_t i=_position+p_size;i>_position;i--){
		int32_t	p=-(i-_position-p_size);
		if (i>=ePixel && i < sPixel ){
			uint32_t pixel;
			if (c_mode==RUN_MODE_COLOR) {
				pixel = hsl2rgbw(pattern_buf[p].h, pattern_buf[p].s, pattern_buf[p].l, pattern_buf[p].w);
			}
			else {
				pixel = hsl2rgbw(pixels[i].h, pixels[i].s, pixels[i].l+ pattern_buf[p].l, pixels[i].w);
			}
			neo_strip->setPixelColor(i,WRGB2COLOR(pixel));
		}
	}

	if (_position<sPixel-p_size && t_mode==RUN_TAIL_OLD){
		//Reset pixel to original color
		uint32_t pixel = hsl2rgbw(pixels[_position+p_size+1].h,pixels[_position+p_size+1].s,pixels[_position+p_size+1].l, pixels[_position+p_size+1].w);
		neo_strip->setPixelColor(_position+p_size+1,WRGB2COLOR(pixel));
	}
	//neo_strip->show();
	_position--;
	if (_position<=ePixel-2*p_size) return true; //Reached end pixel
	else return false;
}

bool cEffect_run::loop(void){
	bool isDone=false;
	if ((_t_elapsed % step_ms) ==0 ){
		switch (dir_mode) {
		case RUN_DIR_RIGHT:
			isDone=shiftRight();
			break;
		case RUN_DIR_LEFT:
			isDone=shiftLeft();
			break;
		case RUN_DIR_PINGPONG:
			if (dir_mode_int==RUN_DIR_RIGHT){
				isDone=shiftRight();
				if (isDone)	{ // Switch direction
					isDone=false;
					dir_mode_int=RUN_DIR_LEFT;
					uint32_t tmp=ePixel;
					ePixel=sPixel;
					sPixel=tmp;
					_position=sPixel;
				}
			} else if (dir_mode_int==RUN_DIR_LEFT) {
				isDone=shiftLeft();
				if (isDone)	{ // Switch direction
					isDone=false;
					dir_mode_int=RUN_DIR_RIGHT;
					uint32_t tmp=ePixel;
					ePixel=sPixel;
					sPixel=tmp;
					_position=sPixel;
				}
			} else {
				isDone=true;
			}
			break;
		default:
			break;
		}
	}
	_t_elapsed+=TICK_INT_MS;
	return !isDone;
}
