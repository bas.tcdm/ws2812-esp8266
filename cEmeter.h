/*
 * e_meter.h
 *
 *  Created on: 15.12.2020
 *      Author: swielens
 */

#ifndef CEMETER_H_
#define CEMETER_H_

#include "cExtDevice.h"

#define EMETER_EEPROM_LAYOUT_VERSION	2

typedef struct {
	int gpio;
	int pulses_per_kwh;
	int update_interval_s;
	char mqtt_topic[MAX_TOPIC_LENTH];
}emeter_settings_t;

typedef struct {
	int crc;
	int layout_version;
	float energy_kwh;
	emeter_settings_t settings;
}eMeter_eeprom_t;

class cEmeter: public cExtDevice {
private:
	 eMeter_eeprom_t eeprom_config;
	 int pulse_cnt, pulse_cnt_last;
	 String parent_topic;
	 float power_kw;
	 int pulse_delta;
	 bool irq;
	 int irq_timeout;

	 unsigned int time_elapsed_ms;
	 int tick_cnt;

	 unsigned int eeprom_address;

	 StaticJsonDocument<150> jsonBuffer;

public:
	cEmeter(unsigned int e_address, String parent_topic, emeter_settings_t default_conf);
	~cEmeter();
	void mqtt_disconnected();
	void mqtt_connected();
	void loop_tick(int intervall_ms);
	void reset_config();
	void mqttMsgHandler(const String &topic, const String &payload);
	void interrupt();
	void emeter_publishResponse(String code, String msg);

	float get_energy(void);
	float get_power(void);
};



#endif /* CEMETER_H_ */
