#include "persistent_storage.h"
#include "utils.h"
#include <EEPROM.h>

//client_config_t ee_config;

//segment_t segments[MAX_NUM_SEGMENTS];

//eeprom_layout_t eeprom_layout;

void eepom_write_buf(uint32_t address, uint8_t *buf, uint32_t n) {
  noInterrupts();
  for (uint32_t i = address; i < address+n; i++) {
    EEPROM.write(i, buf[i-address]);
  }
  EEPROM.commit();
  interrupts();
}

void eeprom_read_buf(uint32_t address, uint8_t *buf, uint32_t n) {
  noInterrupts();
  for (uint32_t i = address; i < address+n; i++) {
    buf[i-address]=EEPROM.read(i);
  }
  interrupts();
}

void eeprom_init(){
  EEPROM.begin(4096);
}

