/*
 * effect_transition.cpp
 *
 *  Created on: 16.11.2020
 *      Author: swielens
 */

#include "cLedStripe.h"
#include "utils.h"

cEffect_transition::cEffect_transition(Adafruit_NeoPixel *strip,  uint32_t pStart, uint32_t pEnd, pixel_t *pixels_Cstart, pixel_t pixels_Cend, uint32_t transition_time_ms){
	neo_strip=strip;
	pixels_current=pixels_Cstart;
	pixels_final=pixels_Cend;
	pixels_end=pEnd;
	pixels_start=pStart;
	t_trans_ms=transition_time_ms;

	SERIAL_PRINTF("Pixels end: %i\n", pixels_end)
	pixels_step=(pixel_t *)malloc(strip->numPixels()*sizeof(pixel_t));

	if (pixels_step==NULL){
		SERIAL_PRINTF("Cannot allocate memory (%i leds) for transition effect\n", strip->numPixels());
		return;
	}

	for (uint32_t i=pStart; i < pEnd;i++){
		pixels_step[i].h=0;
		pixels_step[i].s=0;
		pixels_step[i].l=0;
		pixels_step[i].w=0;
	}

	if (pStart>strip->numPixels()) pixels_start=strip->numPixels();
	if (pEnd>strip->numPixels()) pixels_end=strip->numPixels();

	for (uint32_t i=pStart; i < pEnd;i++){
		pixels_step[i].s=pixels_Cend.s-pixels_Cstart[i].s;
		pixels_step[i].l=pixels_Cend.l-pixels_Cstart[i].l;
		pixels_step[i].w=pixels_Cend.w-pixels_Cstart[i].w;
		pixels_step[i].h=(pixels_Cend.h-pixels_Cstart[i].h<=180000)?pixels_Cend.h-pixels_Cstart[i].h:(pixels_Cend.h-360000-pixels_Cstart[i].h);

		if (pixels_Cend.h>pixels_Cstart[i].h){
			pixels_step[i].h=(pixels_Cend.h-pixels_Cstart[i].h) < (360000-pixels_Cend.h + pixels_Cstart[i].h)?(pixels_Cend.h-pixels_Cstart[i].h):(360000-pixels_Cend.h + pixels_Cstart[i].h);
		} else {
			pixels_step[i].h=(pixels_Cstart[i].h-pixels_Cend.h) < (360000-pixels_Cstart[i].h + pixels_Cend.h)?(pixels_Cend.h-pixels_Cstart[i].h):(360000-pixels_Cstart[i].h + pixels_Cend.h);
		}

		float h=pixels_step[i].h;
		float s=pixels_step[i].s;
		float l=pixels_step[i].l;
		float w=pixels_step[i].w;

		pixels_step[i].h=(int)(h*TICK_INT_MS/t_trans_ms);
		pixels_step[i].s=(int)(s*TICK_INT_MS/t_trans_ms);
		pixels_step[i].l=(int)(l*TICK_INT_MS/t_trans_ms);
		pixels_step[i].w=(int)(w*TICK_INT_MS/t_trans_ms);
		//SERIAL_PRINTF("Pixel step [%i]: %i, %i, %i, %i\n", i, pixels_step[i].h,pixels_step[i].s,pixels_step[i].l,pixels_step[i].w);
	}

	SERIAL_PRINTF("Init Transition object pixel (%i -> %i): (%i, %i, %i, %i) -> (%i, %i, %i, %i), Transition: %i\n", pixels_start, pixels_end,   pixels_Cstart[0].h, pixels_Cstart[0].s, pixels_Cstart[0].l, pixels_Cstart[0].w,  pixels_Cend.h, pixels_Cend.s, pixels_Cend.l,pixels_Cend.w,    t_trans_ms);

	t_current_ms=0;
}

cEffect_transition::~cEffect_transition(){
	free(pixels_step);
}

bool cEffect_transition::loop(){
	//SERIAL_PRINTLN("------------------------");
	for (uint32_t i=pixels_start; i < pixels_end;i++){
		pixels_current[i].h += pixels_step[i].h;
		pixels_current[i].s += pixels_step[i].s;
		pixels_current[i].l += pixels_step[i].l;
		pixels_current[i].w += pixels_step[i].w;

		//if (pixels_step[i].h==0 && pixels_step[i].s==0 && pixels_step[i].l==0 && pixels_step[i].w==0) done++;

		if (pixels_current[i].h<0) pixels_current[i].h=360000;
		if (pixels_current[i].h>360000) pixels_current[i].h=0;
		pixels_current[i].l=constrain(pixels_current[i].l, 0,1000.0);
		pixels_current[i].s=constrain(pixels_current[i].s, 0,1000.0);
		pixels_current[i].w=constrain(pixels_current[i].w, 0,1000.0);

		//SERIAL_PRINTF("[%i]: H=%i, S=%i, L=%i, W=%i\n", i, pixels_step[i].h, pixels_current[i].s,  pixels_current[i].l, pixels_current[i].w);
		uint32_t pixel = hsl2rgbw((float)pixels_current[i].h, (float)pixels_current[i].s, (float)pixels_current[i].l,(float)pixels_current[i].w);
		neo_strip->setPixelColor(i,WRGB2COLOR(pixel));
	}
	//neo_strip->show();
	t_current_ms +=TICK_INT_MS;

	if (t_current_ms>=t_trans_ms) {
		for (uint32_t i=pixels_start; i < pixels_end;i++){
			//Avoid rounding issues
			pixels_current[i].h=pixels_final.h;
			pixels_current[i].l=pixels_final.l;
			pixels_current[i].s=pixels_final.s;
			pixels_current[i].w=pixels_final.w;
			uint32_t pixel = hsl2rgbw((float)pixels_current[i].h, (float)pixels_current[i].s, (float)pixels_current[i].l,(float)pixels_current[i].w);
			neo_strip->setPixelColor(i,WRGB2COLOR(pixel));
		}
		return false;
	}
	return true;
}
