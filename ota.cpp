#include <ESP8266WiFi.h>
#include <ESP8266WiFiMulti.h>
#include <ArduinoOTA.h>
#include "config.h"

void ota_loop(void) {
  ArduinoOTA.handle();
}

void ota_setup() {
  ArduinoOTA.onStart([]() {
    String type;
    if (ArduinoOTA.getCommand() == U_FLASH) {
      type = "sketch";
    } else { // U_FS
      type = "filesystem";
    }

    // NOTE: if updating FS this would be the place to unmount FS using FS.end()
    SERIAL_PRINTLN("Start updating " + type);
  });


  ArduinoOTA.onEnd([]() {
    SERIAL_PRINTLN("\nEnd");
    digitalWrite(LED_BUILTIN, HIGH);
  });

  ArduinoOTA.onProgress([](unsigned int progress, unsigned int total) {
    SERIAL_PRINTF("Progress: %u%%\r\n", (progress / (total / 100)));
    (void)total;(void)progress;
    digitalWrite(LED_BUILTIN, !digitalRead(LED_BUILTIN));
  });

  ArduinoOTA.onError([](ota_error_t error) {
    SERIAL_PRINTF("Error[%u]: ", error);
    if (error == OTA_AUTH_ERROR) {
      SERIAL_PRINTLN("Auth Failed");
    } else if (error == OTA_BEGIN_ERROR) {
      SERIAL_PRINTLN("Begin Failed");
    } else if (error == OTA_CONNECT_ERROR) {
      SERIAL_PRINTLN("Connect Failed");
    } else if (error == OTA_RECEIVE_ERROR) {
      SERIAL_PRINTLN("Receive Failed");
    } else if (error == OTA_END_ERROR) {
      SERIAL_PRINTLN("End Failed");
    }
    digitalWrite(LED_BUILTIN, HIGH);
  });

  ArduinoOTA.begin();
}
