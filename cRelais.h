/*
 * cRelais.h
 *
 *  Created on: 28.12.2020
 *      Author: swielens
 */

#ifndef CRELAIS_H_
#define CRELAIS_H_
#include "cExtDevice.h"

#define RELAIS_EEPROM_LAYOUT_VERSION	3

typedef struct {
	int 	gpio;
	int		invert;
	char 	mqtt_topic[MAX_TOPIC_LENTH];
}relais_settings_t;


typedef struct {
	int 	crc;
	int 	layout_version;
	relais_settings_t settings;
	bool 	isOn;
}relais_eeprom_t;

class cRelais: public cExtDevice {
private:
	 relais_eeprom_t c_eeprom;
	 unsigned int eeprom_address;
	 StaticJsonDocument<150> jsonBuffer;

	 int elapsed_time_ms;

public:
	 cRelais(unsigned int e_address, String base_topic, relais_settings_t cfg);
	~cRelais();
	void mqtt_connected(void);
	void mqtt_disconnected(void);
	void loop_tick(int intervall_ms);
	void reset_config();
	void mqttMsgHandler(const String &topic, const String &payload);
};



#endif /* CRELAIS_H_ */
