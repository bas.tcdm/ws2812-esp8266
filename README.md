**MQTT Interface**

 MQTT_BASE_TOPIC:
 
 Gets published to inform the other clients the configuration. On each change this value gets published
 ```
 {
   "state": "ON" or "OFF",
   "segments": current_num_segments,
   "max_segments": MAX_NUM_SEGMENTS,
   "leds": LED_COUNT
 }
 ```
 
 MQTT_BASE_TOPIC/status:
 
 return value of last command
 ```
 {
   "code": "OK", "NOK", or "READY"
   "msg": <string>
 }
 ```
 
MQTT_BASE_TOPIC/set:

Set segment to color. If segment does not exist, command get ignored and status="ERROR"

```
{
  "state": "ON" or "OFF"
  "brightness": <0...255> -> Optional
  "segment": <0...NUM_SEGMENTS> -> optional, if ommited then all segments will be set to "color"
  "color": "wwrrggbb" -> optional, if ommited then all segments set to previous color
  "base_topic": changing the default base topic -> Optional
}
```
  
MQTT_BASE_TOPIC/add:

Add new segment. See change.
```
{
   "pixel_start": <0..LED_COUNT>,
   "pixel_end":   <0..LED_COUNT>,
   "color":       "rrggbbww" -> Quotes mandatory
 } 
 ```
 
 MQTT_BASE_TOPIC/change: -> change segment
 
 Existing Segment gets changed:
* made bigger: previous segment gets smaller based on pixel_start, next segment gets smaller based on pixel_count
* made smaller: all pixess between previous and pixel_start are off, all pixels after pixel_start + pixel_count are off
* same size but move: if segment conflict with other segment, pixels get reassigned to new segment. if there is a gab between segments, pixels get set to "OFF"
* If segment does not exist, this command gets ignored with status "ERROR"
```
 {
   "segment": <0...MAX_NUM_SEGMENTS>
   "pixel_start":<0..LED_COUNT>
   "pixel_end":<pixel_start..LED_COUNT>
   "color": 0xrrggbbww
}
```

 MQTT_BASE_TOPIC/del: -> delete segment
 
 Segment gets deleted, pixels associated will be in state off. If sigment does not exist, this command will be ignored and status="ERROR"
 ```
 {
   "segment": <0...NUM_SEGMENTS>
 }
```

 MQTT_BASE_TOPIC/reset: -> reset all eeprom settings, no payload

